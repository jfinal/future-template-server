package com.jiubanqingchen.future.org.tool.file;


import cn.hutool.core.date.DateUtil;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.upload.UploadFile;
import com.jiubanqingchen.future.common.tool.FileTool;
import com.jiubanqingchen.future.model.models.Attachment;
import com.jiubanqingchen.future.org.admin.BaseService;

import java.io.File;
import java.util.Date;

/**
 * @ClassName: FileService
 * @Description: (文件处理类)
 * @author Light_Dust
 * @date 2019年11月14日
 */
public class FileService extends BaseService {

    /**
     * @Title: uploadFile
     * @Description:(上传文件 路径: 文件上传配置路径 + 当前日期(yyyyMMdd) + 文件名称)
     * @param @param uploadFile
     * @param @param busiId   业务Id
     * @param @param itemType 业务类型
     * @param @param userId
     * @param @return    参数
     * @return Attachment    返回类型
     * @author Light_dust
     * @date 2019年11月4日
     */
    public static Attachment uploadFile(UploadFile uploadFile, String busiId, String itemType, String userId) {
        String uploadUrl= PropKit.get("uploadUrl");//文件上传配置路径
        String fileName = uploadFile.getFileName();// 文件上传名称
        String extension = fileName.substring(fileName.lastIndexOf(".") + 1);// 得到文件后缀
        long sizes=uploadFile.getFile().length();
        Date now=new Date();
        String folderUrl = "/" + DateUtil.format(now,"yyyy-MM-dd") + "/"+itemType+"/";// 当前文件上传文件夹(日期 +文件上传业务类型)
        FileTool.judgeDirExists(new File(uploadUrl + folderUrl));// 校验上传文件夹是否被创建,未创建则进行创建

        String newName = DateUtil.format(now,"yyyyMMddHHmmss")+fileName.substring(fileName.lastIndexOf("."));//文件重命名
        uploadFile.getFile().renameTo(new File(uploadUrl + folderUrl + newName));//更改文件储存路径


        String size=FileTool.getFileSize(sizes);//获取文件大小

        Attachment attachment = new Attachment();
        attachment.setBusiId(busiId).setFileName(fileName).setItemType(itemType).setFileExt(extension)
                .setFilePath(folderUrl + newName).setSize(size);
        attachment.setFileId(StrKit.getRandomUUID()).setCreateUser(userId).setCreateTime(new Date())
                .setMimeType(uploadFile.getContentType()).save();
        return attachment;
    }

}
