package com.jiubanqingchen.future.org.config;

import com.jfinal.config.Routes;

import com.jiubanqingchen.future.org.tool.file.FileController;
import com.jiubanqingchen.future.org.authority.menu.MenuController;
import com.jiubanqingchen.future.org.business.anonymous.AnonymousController;
import com.jiubanqingchen.future.org.business.attachment.AttachmentController;
import com.jiubanqingchen.future.org.business.basic.BasicController;
import com.jiubanqingchen.future.org.business.login.LoginController;
import com.jiubanqingchen.future.org.business.manager.ManagerController;
import com.jiubanqingchen.future.org.business.module.ModuleController;
import com.jiubanqingchen.future.org.authority.role.RoleController;
import com.jiubanqingchen.future.org.authority.unit.UnitController;
import com.jiubanqingchen.future.org.authority.user.UserController;
import com.jiubanqingchen.future.org.authority.userDept.UserDeptController;
import com.jiubanqingchen.future.org.authority.userGroupRole.UserGroupRoleController;
import com.jiubanqingchen.future.org.business.log.exceptionLog.ExceptionLogController;
import com.jiubanqingchen.future.org.business.log.loginLog.LoginLogController;
import com.jiubanqingchen.future.org.business.log.operateLog.OperateLogController;


public class OrgRoute extends Routes {
    @Override
    public void config() {
        this.add("/org/login", LoginController.class);
        this.add("/org/user", UserController.class);
        this.add("/org/file", FileController.class);
        this.add("/org/attachment", AttachmentController.class);
        this.add("/org/menu", MenuController.class);
        this.add("/org/unit", UnitController.class);
        this.add("/org/module", ModuleController.class);
        this.add("/org/userDept", UserDeptController.class);
        this.add("/org/role", RoleController.class);
        this.add("/org/basic", BasicController.class);
        this.add("/org/userGroupRole", UserGroupRoleController.class);
        this.add("/org/manager", ManagerController.class);
        this.add("/org/loginLog", LoginLogController.class);
        this.add("/org/operateLog", OperateLogController.class);
        this.add("/org/exceptionLog", ExceptionLogController.class);
        this.add("/org/anonymous", AnonymousController.class);
    }
}
