package com.jiubanqingchen.future.cms.index;

import com.jfinal.aop.Clear;
import com.jiubanqingchen.future.org.admin.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Clear
public class IndexController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(IndexController.class);
    public void index() {
        redirect301("https://www.jiubanqingchen.com");
    }
}
