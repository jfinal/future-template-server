package com.jiubanqingchen.future.org.admin;


import com.jiubanqingchen.future.common.enums.SystemStatus;

public class Json {
	
	private int code;
	
	private String msg;
	
	private Object data;

	private boolean succ=true;

	private Long ts=System.currentTimeMillis();
	
	public Json() {
		
	}
	
	public Json(Object data) {
		this.code= SystemStatus.SUCCESS.getCode();
		this.msg=SystemStatus.SUCCESS.getMsg();
		this.data=data;
	}

	public Json(int code, String msg) {
		this.code=code;
		this.msg=msg;
	}
	
	public Json(int code,String msg,Object data) {
		this.code=code;
		this.msg=msg;
		this.data=data;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public boolean isSucc() {
		return succ;
	}

	public void setSucc(boolean succ) {
		this.succ = succ;
	}

	public Long getTs() {
		return ts;
	}

	public void setTs(Long ts) {
		this.ts = ts;
	}
	

}
