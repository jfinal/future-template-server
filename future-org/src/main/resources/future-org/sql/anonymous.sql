###基础查询
#sql("list")
	SELECT 
		`future_anonymous`.*,
		getUserName(`future_anonymous`.createUser) as createUserName,
		getUserName(`future_anonymous`.updateUser) as updateUserName
	FROM 
		future_anonymous AS `future_anonymous`
	WHERE 1=1
		#@anonymousWheres()
		#@anonymousOrderby()
#end

#define anonymousWheres()
	#if(id)
		AND `future_anonymous`.anonymousId=#para(id)
	#end

	#if(url)
		AND `future_anonymous`.url #paraLike(url)
	#end

	#if(createUser)
		AND `future_anonymous`.createUser #paraLike(createUser)
	#end
#end

#define anonymousOrderby()
	#if(prop)
		order by `future_anonymous`.#(prop) #(sort)
	#end
#end