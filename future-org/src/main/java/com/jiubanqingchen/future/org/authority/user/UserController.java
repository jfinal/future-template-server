package com.jiubanqingchen.future.org.authority.user;

import com.jfinal.aop.Clear;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.future.model.models.User;
import com.jiubanqingchen.future.org.admin.ModelController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;
import com.jiubanqingchen.future.org.interceptor.AuthorityInterceptor;
import com.jiubanqingchen.future.org.interceptor.OperateLogInterceptor;

import java.io.File;

/**
 * @author Light_Dust
 * @ClassName: UserController
 * @Description: (系统用户)
 * @date 2019年7月4日 下午1:44:45
 */
@AuthorityKey("org:user")
public class UserController extends ModelController<UserService, User> {

    /**
     * @param
     * @return void    返回类型
     * @throws
     * @Title: info
     * @Description: (得到用户信息)
     * @author Light_Dust
     * @date 2019年7月4日 下午1:44:59
     */
    @AuthorityKey("view")
    public void info() throws Exception {
        User user = getCurrentUser();
        renderResult(new Record().set("user", service.info(user.getUserId())).set("menus", service.getUserTreeMenu(user)));
    }

    /**
     * @param
     * @return void    返回类型
     * @throws
     * @Title: restPass
     * @Description: (重置密码)
     * @author Light_Dust
     * @date 2019年7月4日 下午1:45:10
     */
    @AuthorityKey("update")
    public void restPass() throws Exception {
        Record record = getRecordRawData();
        renderResult(service.restPass(getCurrentUser().getUserId(), record.getStr("oldPassword"), record.getStr("newPassword")));
    }

    @AuthorityKey("export")
    public void export() throws Exception {
        downloadExcel("今夕何夕用户信息", UserExportBean.class, "用户信息", service.exportData(getKv()));
    }

    @AuthorityKey("upload")
    public void importExcel() {
        File file = getFile().getFile();
        service.readExcel(file);
        file.delete();
        renderNull();
    }
}
