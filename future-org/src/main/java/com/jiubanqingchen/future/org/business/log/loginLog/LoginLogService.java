package com.jiubanqingchen.future.org.business.log.loginLog;

import com.jiubanqingchen.future.model.models.LoginLog;
import com.jiubanqingchen.future.org.admin.ModelService;
import com.jiubanqingchen.future.org.annotation.Sqlkey;

@Sqlkey("loginLog.list")
public class LoginLogService extends ModelService<LoginLog> {
	
	
}