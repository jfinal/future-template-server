###基础查询
#sql("list")
	SELECT 
		`future_role`.*
	FROM 
		future_role AS `future_role`
	WHERE 1=1
		#@roleWheres()
		#@roleOrderby()
#end

#define roleWheres()
	#if(id)
		AND `future_role`.roleId=#para(id)
	#end

	#if(roleName)
		AND `future_role`.roleName #paraLike(roleName)
	#end
#end

#define roleOrderby()
    #if(prop)
        order by `future_role`.#(prop) #if(sort) #(sort) #end
    #end
#end

#sql("getRoleAuthorityMenu")
    select DISTINCT resourceId,fm.* from future_resource_authority fra
    INNER JOIN future_menu fm on fra.resourceId=fm.menuId
    where fra.roleId #@genIn(roleIds)
    order by fm.sortNo
#end

###得到当前角色下所有的用户信息 包含在当前角色关联部门中的用户
#sql("getRoleUserIds")
     SELECT userGroupId AS userId FROM	future_user_group_role WHERE userGroupType='USER' AND roleId=?
	    UNION ALL
     SELECT userId FROM future_user_dept fud WHERE
	 EXISTS (SELECT 1 FROM	future_user_group_role fugr WHERE userGroupType='DEPT' AND roleId=? AND fud.deptId = fugr.userGroupId)
#end