package com.jiubanqingchen.future.org.interceptor;

import com.jfinal.aop.Aop;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.JsonKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Record;

import com.jiubanqingchen.future.common.enums.SystemStatus;
import com.jiubanqingchen.future.common.tool.SystemTool;
import com.jiubanqingchen.future.model.models.OperateLog;
import com.jiubanqingchen.future.model.models.User;
import com.jiubanqingchen.future.org.admin.BaseController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;
import com.jiubanqingchen.future.org.business.anonymous.AnonymousService;
import com.jiubanqingchen.future.org.business.log.operateLog.OperateLogService;
import eu.bitwalker.useragentutils.UserAgent;

import javax.servlet.http.HttpServletRequest;

public class OperateLogInterceptor implements Interceptor {
    private Log log = Log.getLog(OperateLogInterceptor.class);

    @Override
    public void intercept(Invocation inv) {
        BaseController controller = (BaseController) inv.getController();
        //校验 是否为匿名访问
        AnonymousService anonymousService = Aop.get(AnonymousService.class);
        boolean flag = anonymousService.judgeIsAnonymous(inv.getActionKey());
        if (!flag) {
            //如果是查看 则不进行操作日志生成
            AuthorityKey authorityKey = inv.getMethod().getAnnotation(AuthorityKey.class);
            if (authorityKey == null) {
                controller.renderResult(SystemStatus.ERROR_AUTHORITY);
                return;
            }
            String methodKey = authorityKey.value();
            if (!"view".equals(methodKey) && 1 == 3) {
                HttpServletRequest request = controller.getRequest();
                new Thread() {
                    @Override
                    public void run() {
                        createOperateLog(inv, controller, request);
                    }
                }.start();
            }
        }
        inv.invoke();
    }

    private void createOperateLog(Invocation inv, BaseController controller, HttpServletRequest request) {
        //转成UserAgent对象
        UserAgent userAgent = SystemTool.getUserAgent(request);

        //获取ip信息
        Record ipInfo = SystemTool.getIpInfo(request);

        //获取用户信息
        User user = controller.getSessionAttr("currentUser");
        //获取页面参数
        Record requestRecord = new Record().set("rawData", controller.getRawData()).set("extData", controller.getRecordExtData());

        //生成操作日志
        OperateLogService operateLogService = Aop.get(OperateLogService.class);
        OperateLog operateLog = new OperateLog();
        operateLog.setLoginArea(ipInfo.getStr("ipArea")).setLoginIp(ipInfo.getStr("ipAddress")).setLoginUserName(user.getUserName()).setRequestData(JsonKit.toJson(requestRecord)).setRequestUrl(inv.getActionKey()).setRequestMethod(request.getMethod())
                .setOs(userAgent.getOperatingSystem().getName()).setBrowser(userAgent.getBrowser().getName());
        try {
            operateLogService.insert(operateLog, new Record(), user);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }
}
