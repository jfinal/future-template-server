package com.jiubanqingchen.future.generator.table;

import com.jfinal.kit.Kv;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

/**
 * @author Light_dust
 * @ClassName: TableService
 * @Description:(查询table相关)
 * @date 2019年9月12日 上午8:59:05
 */
public class TableService {

    private static String tableSchema;

    public TableService() {
        tableSchema = PropKit.get("create_table_schema");
    }

    /**
     * @param @param  tableSchema
     * @param @return 参数
     * @return List<Record>    返回类型
     * @throws
     * @Title: listTable
     * @Description: (得到当前数据库所有table)
     * @author Light_dust
     * @date 2019年9月12日 上午10:21:52
     */
    public Page<Record> page(int pageNum, int limit, Kv kv) throws Exception {
        return Db.template("table.listTable", kv.set("tableSchema", tableSchema)).paginate(pageNum, limit);
    }

    /**
     * @param @param  tableSchema
     * @param @param  tableName
     * @param @return 参数
     * @return List<Record>    返回类型
     * @throws
     * @Title: listColumn
     * @Description: (查询表所有字段)
     * @author Light_dust
     * @date 2019年9月12日 上午10:36:12
     */
    public List<Record> listColumn(String tableName) {
        List<String> notShowTableArr = Arrays.asList(new String[]{"createTime", "createUser", "updateTime", "updateUser"});
        //设置几个默认值
        List<Record> records = Db.find(Db.getSql("table.listColumn"), tableSchema, tableName);
        if (!records.isEmpty()) {
            for (Record record : records) {
                String name = record.getStr("name");
                record.set("dictionaryTypeId",null);
                record.set("queryType",null);
                record.set("showTable", !notShowTableArr.contains(name));
                record.set("showForm", !notShowTableArr.contains(name) ? "input" : null);
                record.set("required", "NO".equals(record.getStr("isNull")));
            }
        }
        return records;
    }

    /**
     * @param @param  tableSchema
     * @param @param  tableName
     * @param @return 参数
     * @return List<Record>    返回类型
     * @throws
     * @Title: getPrimaryColumn
     * @Description: (可能存在多主键)
     * @author Light_dust
     * @date 2019年9月12日 上午10:46:51
     */
    public List<Record> getPrimaryColumn(String tableName) {
        return Db.find(Db.getSql("table.getPrimaryColumn"), tableSchema, tableName);
    }

    /**
     * @param @param  tableSchema
     * @param @param  tableName
     * @param @return 参数
     * @return Record    返回类型
     * @throws
     * @Title: queryTableByName
     * @Description: (查询table详细信息)
     * @author Light_dust
     * @date 2019年9月16日 下午1:49:24
     */
    public Record queryTableByName(String tableName) {
        return Db.findFirst(Db.getSql("table.getTableByName"), tableSchema, tableName);
    }
}
