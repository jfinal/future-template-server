package com.jiubanqingchen.future.common.tool;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * sql处理工具类
 * @author light_dust
 * @Date 2020/5/23
 */
public class SqlTool {

    /**
     * 处理orderby参数
     *
     * @return
     */
    public static Kv orderHandle(Kv kv) {
        String orders = kv.getStr("orders");
        if (StrKit.notBlank(orders)) {
            JSONObject jb = JSONObject.parseObject(orders);
            String order = jb.getString("order");
            String field = jb.getString("field");
            if (StrKit.notBlank(field)) {
                if ("descend".equals(order)) {
                    order = "desc";
                } else {
                    order = "asc";
                }
                kv.set("prop", field);
                kv.set("sort", order);
            }
            kv.delete("orders");
        }
        return kv;
    }

    /**
     * 处理需要隐藏的字段
     *
     * @param list        需要进行去除的类
     * @param hideColumns 需要隐藏的列 a,b,c
     */
    public static void hideColumnsTool(List<Record> list, String hideColumns) {
        if (hideColumns != null && list.size() > 0) {
            //我们需要删除这些属性
            String[] hideColumnArray = hideColumns.split(",");
            for (Record record : list) {
                record.remove(hideColumnArray);
            }
        }
    }

    /**
     * @Title: getSqlString
     * @Description: (得到带''的sql varchar)
     * @param @param str
     * @param @return 参数
     * @return String 返回类型
     * @throws @author Light_Dust
     * @date 2019年5月10日 下午9:52:40
     */
    public static String getSqlString(String str) {
        return "'" + str + "'";
    }

}
