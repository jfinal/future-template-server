###附件表基础查询
#sql("list")
	SELECT 
		`future_attachment`.*
	FROM
        future_attachment AS `future_attachment`
	WHERE 1=1
		#@attachmentWheres()
		#@attachmentOrderby()
#end

#define attachmentWheres()
	#if(id)
		AND `future_attachment`.fileId=#para(id)
	#end
    #if(createTime)
        AND `future_attachment`.createTime>=#para(createTime)
    #end
#end

#define attachmentOrderby()
	order by `future_attachment`.createTime desc
#end