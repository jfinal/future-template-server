package com.jiubanqingchen.future.generator.create;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Inject;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.future.org.admin.BaseController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;

import java.util.HashMap;
import java.util.Map;

@AuthorityKey("cms:table")
public class CreateController extends BaseController {
    @Inject
    CreateService createService;

    @AuthorityKey("add")
    public void index() {
        JSONObject jb = JSON.parseObject(getRawData());
        Record createForm= new Record().setColumns(JsonKit.parse(jb.getString("createForm"), HashMap.class));
        createForm.set("columns",JSON.parseArray(jb.getString("columns"), Map.class));
        createService.create(createForm);
        renderResult(true);
    }

    public void preview(){
        JSONObject jb = JSON.parseObject(getRawData());
        Record createForm= new Record().setColumns(JsonKit.parse(jb.getString("createForm"), HashMap.class));
        createForm.set("columns",JSON.parseArray(jb.getString("columns"), Map.class));
    }
}
