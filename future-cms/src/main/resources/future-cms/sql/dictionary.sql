#sql("list")
SELECT 
	`future_dictionary`.*
	#if(join)
	,`future_dictionary_type`.codeItemName
	#end
FROM
    future_dictionary AS `future_dictionary`
	#if(join)
		INNER JOIN future_dictionary_type as `future_dictionary_type`
		ON `future_dictionary`.codeItemId=`future_dictionary_type`.codeItemId
	#end
WHERE 1=1
	#@dictionaryWheres()
ORDER BY
	`future_dictionary`.sortNo ASC
#end

#define dictionaryWheres()
	#if(id)
		AND `future_dictionary`.dictionaryId=#para(id)
	#end
	
	#if(likeAll)
		AND `future_dictionary`.codeId #paraLike(likeAll) or `future_dictionary`.codeName #paraLike(likeAll)
	#end
	
	#if(codeItemId)
		AND `future_dictionary`.codeItemId =#para(codeItemId)
	#end
#end