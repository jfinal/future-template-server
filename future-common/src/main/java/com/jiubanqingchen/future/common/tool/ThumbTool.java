package com.jiubanqingchen.future.common.tool;

import net.coobird.thumbnailator.Thumbnails;

public class ThumbTool {

    /**
     *按比例压缩图片
     * @param scale 比例
     * @param fileUrl 文件url
     * @param thumbUrl 缩略图url
     * @throws Exception
     */
    public static void thumbScale(double scale,String fileUrl,String thumbUrl) throws Exception{
        Thumbnails.of(fileUrl).scale(scale).toFile(thumbUrl);
    }

    public static void thumbWH(int maxWidth,int maxHeight,String fileUrl,String thumbUrl) throws Exception{
        Thumbnails.of(fileUrl).size(maxWidth,maxHeight).toFile(thumbUrl);
    }
}
