package com.jiubanqingchen.future.org.admin;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.TableMapping;
import com.jiubanqingchen.future.model.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.List;

public class ModelService<T extends Model> extends BaseService implements IModelService<T> {
    private static Logger logger = LoggerFactory.getLogger(ModelService.class);
    private Class<T> modelClazz;

    public ModelService() {
    }

    protected void getModelClazz() {
        modelClazz = (Class) ((ParameterizedType) this.getClass().getSuperclass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public void afterDelete(T model, Record record, User _usr) throws Exception {

    }

    @Override
    public void afterInsert(T model, Record record, User _usr) throws Exception {

    }

    @Override
    public void afterSave(T model, Record record, User _usr) throws Exception {

    }

    @Override
    public void afterUpdate(T model, Record record, User _usr) throws Exception {

    }

    @Override
    public boolean delete(T model, Record record, User _usr) throws Exception {
        if (_usr == null) throw new Exception("操作用户信息为空,不予删除");
        preDelete(model, record, _usr);
        if (!model.delete()) {
            return false;
        }
        afterDelete(model, record, _usr);
        return true;
    }

    @Override
    public boolean deleteBatch(List<T> modelList, Record record, User _usr) throws Exception {
        for (T model : modelList) {
            delete(model, record, _usr);
        }
        return true;
    }

    @Override
    public T insert(T model, Record record, User _usr) throws Exception {
        if (_usr == null) throw new Exception("操作用户信息为空,不予新增");
        try {
            preInsert(model, record, _usr);
            Class clazz = model.getClass();
            String primaryKey = TableMapping.me().getTable(clazz).getPrimaryKey()[0];
            String pk = model.getStr(primaryKey);
            if (pk == null || "".equals(pk)) {// 主键为空,生成主键id
                model.set(primaryKey, StrKit.getRandomUUID());
            }
            model.set("createTime", new Date()).set("createUser", _usr.getUserId()).save();
            afterInsert(model, record, _usr);
            afterSave(model, record, _usr);
            return model;
        } catch (Exception e) {
            logger.error("insert异常:  " + e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public boolean insertBatch(List<T> modelList, Record record, User _usr) throws Exception {
        for (T model : modelList) {
            this.insert(model, record, _usr);
        }
        return true;
    }

    @Override
    public void preDelete(T model, Record record, User _usr) throws Exception {

    }

    @Override
    public void preInsert(T model, Record record, User _usr) throws Exception {

    }

    @Override
    public void preUpdate(T model, Record record, User _usr) throws Exception {

    }

    @Override
    public T update(T model, Record record, User _usr) throws Exception {
        if (_usr == null) throw new Exception("操作用户信息为空,不予修改");
        try {
            preUpdate(model, record, _usr);
            model.set("updateTime", new Date());
            model.set("updateUser", _usr.getUserId());
            model.update();
            afterUpdate(model, record, _usr);
            afterSave(model, record, _usr);
            return model;
        } catch (Exception e) {
            logger.error("update异常:  " + e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public boolean updateBatch(List<T> modelList, Record record, User _usr) throws Exception {
        for (T model : modelList) {
            this.update(model, record, _usr);
        }
        return true;
    }

    public String getTableName(Class<? extends Model> modelCls) {
        return TableMapping.me().getTable(modelCls).getName();
    }


}
