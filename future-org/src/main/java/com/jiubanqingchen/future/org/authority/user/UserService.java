package com.jiubanqingchen.future.org.authority.user;

import com.alibaba.excel.EasyExcel;
import com.jfinal.aop.Aop;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jiubanqingchen.future.common.enums.CacheName;
import com.jiubanqingchen.future.common.tool.MD5Tool;
import com.jiubanqingchen.future.model.models.Attachment;
import com.jiubanqingchen.future.model.models.User;
import com.jiubanqingchen.future.model.models.UserDept;
import com.jiubanqingchen.future.model.models.UserGroupRole;
import com.jiubanqingchen.future.org.admin.ModelService;
import com.jiubanqingchen.future.org.annotation.Sqlkey;
import com.jiubanqingchen.future.org.business.attachment.AttachmentService;
import com.jiubanqingchen.future.org.authority.userDept.UserDeptService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * @author Light_Dust
 * @ClassName: UserService
 * @Description: (管理员userService)
 * @date 2019年7月4日 下午1:38:23
 */
@Sqlkey("user.list")
public class UserService extends ModelService<User> {

    private static Logger logger = LoggerFactory.getLogger(UserService.class);

    public User info(String userId) {
        User user = CacheKit.get(CacheName.USER_LIST.getValue(), userId);
        return user != null ? user : User.dao.findById(userId);
    }

    /**
     * @param user
     * @throws Exception
     * @author Light_Dust
     * @date 2019年7月4日 下午1:38:47
     */
    @Override
    public void preInsert(User user, Record record, User _usr) throws Exception {
        // 用户新增前初始化密码为123456
        user.setUserPassword(MD5Tool.md5("123456"));
        super.preInsert(user, record, _usr);
    }

    @Override
    public void afterInsert(User user, Record record, User _usr) throws Exception {
        String unitId = record.get("unitId");
        //包含unitId添加部门关联
        if (unitId != null) {
            UserDeptService userDeptService = Aop.get(UserDeptService.class);
            UserDept userDept = new UserDept();
            userDept.setUserId(user.getUserId());
            userDept.setDeptId(unitId);
            userDeptService.insert(userDept, record, _usr);
        }
        super.afterInsert(user, record, _usr);
    }

    /**
     * @param @param  userId
     * @param @param  password
     * @param @param  newPassword
     * @param @return 参数
     * @return boolean 返回类型
     * @throws @author Light_Dust
     * @Title: restPass
     * @Description: (重置密码)
     * @date 2019年6月19日 下午7:08:06
     */
    public boolean restPass(String userId, String oldpassword, String newPassword) throws Exception {
        User user = User.dao.findById(userId);
        if (user == null) {
            throw new Exception("用户信息异常");
        }
        if (!MD5Tool.md5(oldpassword).equals(user.getUserPassword())) {
            throw new Exception("旧密码错误");
        }
        return user.setUserPassword(MD5Tool.md5(newPassword)).setUpdateTime(new Date()).update();
    }


    @Override
    public void afterDelete(User user, Record record, User _usr) throws Exception {
        //被删除人id
        String delUserId = user.getUserId();

        logger.info("删除用户：" + user.getUserName() + " 图片数据");
        AttachmentService attachmentService = Aop.get(AttachmentService.class);
        Attachment attachment = Attachment.dao.findById(user.getUserAvatar());
        if (attachment != null) attachmentService.delete(attachment, record, _usr);

        logger.info("删除用户：" + user.getUserName() + " 部门关联数据");
        delete(getTableName(UserDept.class), "userId", delUserId, _usr);

        logger.info("删除用户：" + user.getUserName() + " 角色关联数据");
        delete(getTableName(UserGroupRole.class), "userGroupId", delUserId, _usr);

        super.afterDelete(user, record, _usr);
    }

    /**
     * 获取用户菜单
     *
     * @return
     * @throws Exception
     */
    public List<Record> getUserTreeMenu(User user) throws Exception {
        UserAuthorityService userAuthorityService = new UserAuthorityService();
        return userAuthorityService.getUserAuthorityMenu(user.getUserId());
    }

    @Override
    public List<UserExportBean> exportData(Kv kv) throws Exception {
        return this.exportData("user.exportUser", kv, UserExportBean.class);
    }

    public void readExcel(File file) {
        EasyExcel.read(file, UserExportBean.class, new UserImportListener()).sheet().doRead();
    }
}


