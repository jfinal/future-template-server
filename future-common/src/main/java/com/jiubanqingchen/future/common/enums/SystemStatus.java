package com.jiubanqingchen.future.common.enums;

/**
 * @ClassName: SystemStatus  
 * @Description: (系统通用状态码)  
 * @author Light_Dust  
 * @date 2019年7月4日 下午1:53:28
 */
public enum SystemStatus {
/**--------------------------------------------自定义状态码----------------------------------------------*/

	LOGIN_SUCCESS(10001,"登录成功"),
	
	LOGIN_ERROR(10002,"账号或者密码错误"),
	
	ERROR_AUTHORITY(10003,"资源不可用,请联系管理员设置权限"),

	LOGIN_MORE(10004,"您已在别处登录,请注意密码信息是否泄露"),
	
	LOGOUT(10005,"当前登录已失效,请重新登录"),

	CAPTCHA_ERROR(10006,"验证码错误"),
	
	ERROR(50000,"系统异常,请联系管理员"),
	
/**--------------------------------------------表格处理状态码----------------------------------------------*/
	
	SUCCESS(60000, "操作成功"),
	
	INSERT_SUCCESS(60001,"新增成功"),
	
	DELETE_SUCCESS(60002,"删除成功"),
	
	UPDATE_SUCCESS(60003,"修改成功");
	
	private Integer code;

	private String msg;

	private SystemStatus(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public static String getName(Integer code) {
		for (SystemStatus sysCode : SystemStatus.values()) {
			if (sysCode.getCode() == code) {
				return sysCode.msg;
			}
		}
		return null;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
