#sql("list")
SELECT 
	`future_cron_task`.*
FROM
    future_cron_task AS `future_cron_task`
WHERE 1=1
  #@cronTaskWheres()
  #@cronTaskOrderby()
#end

#define cronTaskWheres()
	#if(id)
	    AND `future_cron_task`.cronTaskId=#para(id)
	#end

    #if(active)###查询任务是否启动
        AND `future_cron_task`.active=#(active)
    #end
#end

#define cronTaskOrderby()
    #if(prop)
        order by `future_cron_task`.#(prop) #if(sort) #(sort) #end
    #else
        order by `future_cron_task`.cronTaskNo
    #end
#end