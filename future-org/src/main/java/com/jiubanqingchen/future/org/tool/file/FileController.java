package com.jiubanqingchen.future.org.tool.file;


import com.jfinal.aop.Clear;
import com.jfinal.aop.Inject;
import com.jfinal.kit.PropKit;
import com.jfinal.upload.UploadFile;
import com.jiubanqingchen.future.common.enums.SystemStatus;
import com.jiubanqingchen.future.common.tool.FileTool;
import com.jiubanqingchen.future.model.models.Attachment;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;
import com.jiubanqingchen.future.org.admin.BaseController;
import com.jiubanqingchen.future.org.interceptor.OperateLogInterceptor;

import java.io.File;

/**
 * @ClassName: FileController
 * @Description: (文件处理类)
 * @author Light_Dust
 * @date 2019年11月14日
 */
@AuthorityKey("org:file")
@Clear(OperateLogInterceptor.class)
public class FileController extends BaseController {

    @Inject
    FileService fileService;
    /**
     * @Title: uploadImg
     * @Description: (图片上传)
     * @param
     * @return void 返回类型
     * @throws @author Light_Dust
     * @date 2019年6月17日 下午10:36:45
     */
    @AuthorityKey("add")
    public void uploadImg() {
        UploadFile uploadFile = getFile();
        if (!FileTool.isImage(uploadFile.getFile())) {
            renderResult(SystemStatus.ERROR, "只能上传图片类型");
            return ;
        }
        Attachment attachment = fileService.uploadFile(uploadFile, getPara("busiId"), getPara("itemType"),
                getCurrentUser().getUserId());
        renderResult(SystemStatus.SUCCESS, attachment);
    }

    /**
     * @Title: downloadFile
     * @Description: (下载文件 需要fileId)
     * @param
     * @return void 返回类型
     * @throws @author Light_Dust
     * @date 2019年7月4日 下午1:42:11
     */
    @Clear
    public void downloadFile() {
        String fileId = getPara("fileId");
        Attachment attachment = Attachment.dao.findById(fileId);
        if (attachment != null) {
            renderFile(new File(PropKit.get("uploadUrl") + attachment.getFilePath()));
        } else {
            renderResult(SystemStatus.ERROR, "文件不存在");
        }
    }

    @AuthorityKey("add")
    public void upload(){
        UploadFile uploadFile = getFile();
        Attachment attachment = fileService.uploadFile(uploadFile, getPara("busiId"), getPara("itemType"),
                getCurrentUser().getUserId());
        renderResult(SystemStatus.SUCCESS, attachment);
    }
}
