package com.jiubanqingchen.future.common.tool;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UploadTool {

	/**
	 * @Title: rename
	 * @Description: (修改文件名称)
	 * @param @param  fileName
	 * @param @return 参数
	 * @return String 返回类型
	 * @throws @author Light_Dust
	 * @date 2019年6月17日 下午8:36:44
	 */
	public static String rename(String fileName) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		String extension = fileName.substring(fileName.lastIndexOf("."));// 得到文件后缀名
		return sdf.format(new Date()) + extension;
	}

}
