package com.jiubanqingchen.future.org.business.module;

import com.jiubanqingchen.future.model.models.Module;
import com.jiubanqingchen.future.org.admin.ModelService;
import com.jiubanqingchen.future.org.annotation.Sqlkey;

@Sqlkey("module.list")
public class ModuleService extends ModelService<Module> {

}