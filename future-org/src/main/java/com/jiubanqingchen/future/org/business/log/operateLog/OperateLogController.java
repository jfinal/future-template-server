package com.jiubanqingchen.future.org.business.log.operateLog;

import com.jfinal.aop.Clear;
import com.jiubanqingchen.future.model.models.OperateLog;
import com.jiubanqingchen.future.org.admin.ModelController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;
import com.jiubanqingchen.future.org.interceptor.OperateLogInterceptor;

@AuthorityKey("org:operateLog")
@Clear(OperateLogInterceptor.class)
public class OperateLogController extends ModelController<OperateLogService, OperateLog> {

}