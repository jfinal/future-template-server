###基础查询
#sql("list")
	SELECT 
		`future_login_log`.*
	FROM 
		future_login_log AS `future_login_log`
	WHERE 1=1
		#@loginLogWheres()
		#@loginLogOrderby()
#end

#define loginLogWheres()
	#if(id)
		AND `future_login_log`.loginLogId=#para(id)
	#end
	#if(loginUserName)
		AND `future_login_log`.loginUserName LIKE concat('%',#para(loginUserName),'%')
	#end
	#if(loginIp)
		AND `future_login_log`.loginIp LIKE concat('%',#para(loginIp),'%')
	#end
	#if(loginArea)
		AND `future_login_log`.loginArea LIKE concat('%',#para(loginArea),'%')
	#end
#end

#define loginLogOrderby()
	#if(prop)
		order by `future_login_log`.#(prop) #(sort)
    #else
        order by `future_login_log`.createTime desc
	#end
#end