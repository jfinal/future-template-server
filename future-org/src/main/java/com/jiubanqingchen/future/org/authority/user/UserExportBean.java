package com.jiubanqingchen.future.org.authority.user;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;

import java.util.Date;
@ContentRowHeight(30)//内容高度
@HeadRowHeight(30)//头部高度
@ColumnWidth(25)//列宽
public class UserExportBean {

    @ExcelProperty({"今夕何夕2020用户表","账户"})
    private String userAccount;

    @ExcelProperty({"今夕何夕2020用户表","姓名"})
    private String userName;

    @ExcelProperty({"今夕何夕2020用户表","电话"})
    private String userPhone;

    @ColumnWidth(40)
    @ExcelProperty({"今夕何夕2020用户表","邮箱"})
    private String userEmail;

    @DateTimeFormat("yyyy年MM月dd日")
    @ExcelProperty({"今夕何夕2020用户表","生日"})
    private Date userBrithday;

    @ExcelProperty({"今夕何夕2020用户表","性别"})
    private String userSexDesc;

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Date getUserBrithday() {
        return userBrithday;
    }

    public void setUserBrithday(Date userBrithday) {
        this.userBrithday = userBrithday;
    }

    public String getUserSexDesc() {
        return userSexDesc;
    }

    public void setUserSexDesc(String userSexDesc) {
        this.userSexDesc = userSexDesc;
    }
}
