/*
 Navicat Premium Data Transfer

 Source Server         : future
 Source Server Type    : MySQL
 Source Server Version : 80017
 Source Host           : 39.108.1.31:3306
 Source Schema         : future_template

 Target Server Type    : MySQL
 Target Server Version : 80017
 File Encoding         : 65001

 Date: 24/07/2020 09:58:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for future_anonymous
-- ----------------------------
DROP TABLE IF EXISTS `future_anonymous`;
CREATE TABLE `future_anonymous`  (
  `anonymousId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '匿名访问id',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路径',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`anonymousId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '匿名访问' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_anonymous
-- ----------------------------
INSERT INTO `future_anonymous` VALUES ('ce118b93f5494cdab92c280e5ed521eb', '/org/anonymous/test', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-31 17:42:53', '21dbcbb7889840359426b368c74a9375', '2020-07-14 15:27:00');

-- ----------------------------
-- Table structure for future_attachment
-- ----------------------------
DROP TABLE IF EXISTS `future_attachment`;
CREATE TABLE `future_attachment`  (
  `fileId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件id',
  `fileName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `mimeType` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件mime',
  `fileExt` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件后缀名',
  `filePath` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件路径',
  `busiId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联id',
  `itemType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务类型',
  `size` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件大小',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`fileId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_attachment
-- ----------------------------
INSERT INTO `future_attachment` VALUES ('0077f6f795c047ebaf3b1b747aa061f8', 'qc.jpg', 'image/jpeg', 'jpg', '/2020-07-13/avatar/20200713102849.jpg', NULL, 'avatar', '26.1 kB', 'a8121140323b4200aee4630e9f191379', '2020-07-13 10:28:50', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('06a9b90d198749abb3f475e370ae8027', 'u=1906469856,4113625838&fm=26&gp=0.jpg', 'image/jpeg', 'jpg', '/2020-05-22/avatar/20200522194612.jpg', NULL, 'avatar', '28.9 kB', '3752e7955b6444e9b9327b942c54861b', '2020-05-22 19:46:13', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('0c5084daf1df4c8dbf274fb3de8279a1', '20191110100252.jpg', 'image/jpeg', 'jpg', '/2020-01-08/avatar/20200108191519.jpg', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', 'avatar', '26.1 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-01-08 19:15:19', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('184d5eb858494bd09c800cf6d6192c84', '11.jpg', 'image/jpeg', 'jpg', '/2019-11-22/newsTitle/20191122183534.jpg', NULL, 'newsTitle', '1.6 MB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-11-22 18:35:34', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('1caa66fc8beb4d75b21efed83bf8baa2', 'qc.jpg', 'image/jpeg', 'jpg', '/2020-04-27/avatar/20200427154548.jpg', NULL, 'avatar', '26.1 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-04-27 15:45:49', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('1e821daa4138418fa1e69460d74e7c14', 'qc4.jpg', 'image/jpeg', 'jpg', '/2020-06-05/avatar/2020-06-0520:26:59.jpg', NULL, 'avatar', '26.1 kB', '21dbcbb7889840359426b368c74a9375', '2020-06-05 20:27:57', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('2c7afc27786348a2921b60fc26521232', '55.jpg', 'image/jpeg', 'jpg', '/2019-11-22/newsTitle/20191122184734.jpg', NULL, 'newsTitle', '264.8 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-11-22 18:47:35', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('2d578870828f4a969d12158c272ae1d2', 'qc.jpg', 'image/jpeg', 'jpg', '/2020-04-27/avatar/20200427154713.jpg', NULL, 'avatar', '26.1 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-04-27 15:47:14', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('2e3f9fe7d9e642e5a0fdadb581df9f13', 'qc.jpg', 'image/jpeg', 'jpg', '/2019-11-10/avatar/20191110100252.jpg', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', 'avatar', NULL, 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-11-10 22:02:52', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('3046e424c6c94616a928f35304a27394', 'qc.jpg', 'image/jpeg', 'jpg', '/2020-04-27/avatar/20200427154946.jpg', NULL, 'avatar', '26.1 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-04-27 15:49:47', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('4db9c74e6ab54188a64dd39ef4119a6e', 'qc.jpg', 'image/jpeg', 'jpg', '/2020-05-18/testtype/20200518192448.jpg', 'test', 'testtype', '26.1 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-18 19:24:48', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('5377684227a746109d920acc1a99eaf3', 'qc.jpg', 'image/jpeg', 'jpg', '/2020-05-14/avatar/20200514193828.jpg', NULL, 'avatar', '26.1 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-14 19:38:29', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('5873c8458f0d466a9426eb465cd12713', '41425495efae5e709ac3da29c64723e6.jpeg', 'image/jpeg', 'jpeg', '/2020-07-13/avatar/20200713101223.jpeg', NULL, 'avatar', '147.9 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-13 10:12:24', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('698da8c16eca4834ac58141425b7b13d', 'qc.jpg', 'image/jpeg', 'jpg', '/2020-04-27/avatar/20200427155252.jpg', NULL, 'avatar', '26.1 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-04-27 15:52:52', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('69eb09619c9b48c8a20b52a4a89a9f16', 'qc.jpg', 'image/jpeg', 'jpg', '/2020-05-14/avatar/20200514193929.jpg', NULL, 'avatar', '26.1 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-14 19:39:29', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('75f988b150ed4aba83be84022cb517df', 'qc.jpg', 'image/jpeg', 'jpg', '/2020-07-02/avatar/20200702200021.jpg', NULL, 'avatar', '26.1 kB', '21dbcbb7889840359426b368c74a9375', '2020-07-02 20:00:22', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('80c0bb3a301f46439b8b357ddddcc39b', '22.jpg', 'image/jpeg', 'jpg', '/2019-11-22/newsTitle/20191122183559.jpg', NULL, 'newsTitle', '2.2 MB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-11-22 18:36:00', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('8d6eba9709034e6b90b2ca223819b16c', '33.jpg', 'image/jpeg', 'jpg', '/2019-11-22/newsTitle/20191122184712.jpg', NULL, 'newsTitle', '263.7 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-11-22 18:47:12', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('90a069baf1184334b7f712d3fd6752b3', 'qc5.jpg', 'image/jpeg', 'jpg', '/2020-06-05/avatar/20200605203305.jpg', NULL, 'avatar', '26.1 kB', '21dbcbb7889840359426b368c74a9375', '2020-06-05 20:33:05', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('9503e22f95d84b95babf85cb78ab2909', '44.jpg', 'image/jpeg', 'jpg', '/2019-11-22/newsTitle/20191122184723.jpg', NULL, 'newsTitle', '305.2 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-11-22 18:47:23', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('9c3152e5f9aa4e8c9d40f8db6fbef5a3', '20191110100252.jpg', 'image/jpeg', 'jpg', '/2020-01-06/avatar/20200106162023.jpg', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', 'avatar', '26.1 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-01-06 16:20:23', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('9edd68a3eaf54360b134ab3a3c88e8be', '66.jpg', 'image/jpeg', 'jpg', '/2019-11-22/newsTitle/20191122185037.jpg', NULL, 'newsTitle', '85.8 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-11-22 18:50:37', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('9f54e99352644aab9af77b10c0377a64', '20191110100252.jpg', 'image/jpeg', 'jpg', '/2020-01-08/avatar/20200108180318.jpg', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', 'avatar', '26.1 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-01-08 18:03:18', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('ad29787aea614aecb5359fd7a5337298', 'qc.jpg', 'image/jpeg', 'jpg', '/2020-06-30/avatar/20200630151207.jpg', NULL, 'avatar', '26.1 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-06-30 15:12:07', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('ad49d350bc39406f835d41a5b7a8dd77', '新建文本文档 (2).txt', 'text/plain', 'txt', '/2020-05-18/testtype/20200518192810.txt', 'test', 'testtype', '957 B', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-18 19:28:11', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('b9d5075d817a4ce9af58bff17ab74817', 'qc.jpg', 'image/jpeg', 'jpg', '/2020-06-05/avatar/2020-06-05 20:22:00.jpg', NULL, 'avatar', '26.1 kB', '21dbcbb7889840359426b368c74a9375', '2020-06-05 20:22:00', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('c8c467c5325c46b29a021298c2883e13', 'qc.jpg', 'image/jpeg', 'jpg', '/2020-07-02/avatar/20200702195509.jpg', NULL, 'avatar', '26.1 kB', '21dbcbb7889840359426b368c74a9375', '2020-07-02 19:55:10', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('d13b2be1faf2422a86dc8726ead67eb8', 'qc.jpg', 'image/jpeg', 'jpg', '/2020-04-27/avatar/20200427154121.jpg', NULL, 'avatar', '26.1 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-04-27 15:41:22', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('d2d80a7533134f3e858f1d97e7fb7275', '77.jpg', 'image/jpeg', 'jpg', '/2019-11-22/newsTitle/20191122185045.jpg', NULL, 'newsTitle', '166.1 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-11-22 18:50:45', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('d3f9d20438f0444d8cf5e2839353532c', 'qc5.jpg', 'image/jpeg', 'jpg', '/2020-06-05/avatar/1591360173067.jpg', NULL, 'avatar', '26.1 kB', '21dbcbb7889840359426b368c74a9375', '2020-06-05 20:29:33', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('debd38df5fe54fd2894502fd6f70a07d', 'u=2434036295,1423187634&fm=26&gp=0.jpg', 'image/jpeg', 'jpg', '/2020-05-20/avatar/20200520195953.jpg', NULL, 'avatar', '25.7 kB', '3752e7955b6444e9b9327b942c54861b', '2020-05-20 19:59:53', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('e0f5c2c700a943689483bbd73dbfbba5', 'qc3.jpg', 'image/jpeg', 'jpg', '/2020-06-05/avatar/2020-06-0520:26:31.jpg', NULL, 'avatar', '26.1 kB', '21dbcbb7889840359426b368c74a9375', '2020-06-05 20:26:32', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('e1901611dae74bdc8e4eaef00934c396', '火狐截图_2020-07-11T02-20-25.154Z.png', 'image/png', 'png', '/2020-07-17/avatar/20200717094017.png', NULL, 'avatar', '19.3 kB', 'a8121140323b4200aee4630e9f191379', '2020-07-17 09:40:18', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('e2746654f6f048a78be03184ad1a7298', 'qc1.jpg', 'image/jpeg', 'jpg', '/2020-06-05/avatar/2020-06-05 20:22:30.jpg', NULL, 'avatar', '26.1 kB', '21dbcbb7889840359426b368c74a9375', '2020-06-05 20:22:30', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('e2b36c817e684bf59456998409f31053', '20191110100252.jpg', 'image/jpeg', 'jpg', '/2020-01-06/avatar/20200106174514.jpg', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', 'avatar', '26.1 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-01-06 17:45:14', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('e94461a344384ecd8183d9b0b6977bf2', 'qc.jpg', 'image/jpeg', 'jpg', '/2020-06-30/avatar/20200630144031.jpg', NULL, 'avatar', '26.1 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-06-30 14:40:31', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('ed66f352472548e282f3c2c1eeb7cf63', 'qc2.jpg', 'image/jpeg', 'jpg', '/2020-06-05/avatar/2020-06-0520:24:28.jpg', NULL, 'avatar', '26.1 kB', '21dbcbb7889840359426b368c74a9375', '2020-06-05 20:24:28', NULL, NULL);
INSERT INTO `future_attachment` VALUES ('ee7544ce142440e88f950f8099aed3e1', 'qc.jpg', 'image/jpeg', 'jpg', '/2020-06-05/avatar/2020-06-05 17:48:35.jpg', NULL, 'avatar', '26.1 kB', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-06-05 17:48:36', NULL, NULL);

-- ----------------------------
-- Table structure for future_config
-- ----------------------------
DROP TABLE IF EXISTS `future_config`;
CREATE TABLE `future_config`  (
  `configId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '配置id',
  `configName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置名称',
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '配置json',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`configId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_config
-- ----------------------------
INSERT INTO `future_config` VALUES ('045b4eec565f4ed8a1dc433128e8cd27', '111', '1111', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-27 17:55:32', NULL, NULL);
INSERT INTO `future_config` VALUES ('87f0e6aa14fa4fa889a259aec7e2280b', '22', '222', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-27 17:55:41', NULL, NULL);

-- ----------------------------
-- Table structure for future_cron_task
-- ----------------------------
DROP TABLE IF EXISTS `future_cron_task`;
CREATE TABLE `future_cron_task`  (
  `cronTaskId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务id',
  `cronTaskNo` int(11) NOT NULL COMMENT '任务编号',
  `cronTaskName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `cronExpression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务表达式',
  `className` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '执行类',
  `active` tinyint(1) NULL DEFAULT NULL COMMENT '是否激活',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`cronTaskId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_cron_task
-- ----------------------------
INSERT INTO `future_cron_task` VALUES ('3a72e855285c4b52bfca39b2ce138094', 1, 'Hello 轻尘', 'Hello 久伴轻尘', '0/40 * * * * ? *', 'com.jiubanqingchen.future.org.tool.job.PintWordJob', 0, 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-21 22:50:17', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-06-05 14:41:26');

-- ----------------------------
-- Table structure for future_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `future_dictionary`;
CREATE TABLE `future_dictionary`  (
  `dictionaryId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典主键',
  `codeItemId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型ID',
  `codeId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典ID',
  `codeName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `sortNo` int(8) NULL DEFAULT NULL COMMENT '排序',
  `isUpdate` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否维护',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`dictionaryId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据字典' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_dictionary
-- ----------------------------
INSERT INTO `future_dictionary` VALUES ('23695a18a5ae449dbb0bb54626c7ef67', 'CREATE_SHOW_FORM_TYPE', 'input', '输入框', 1, NULL, '2019-12-25 21:38:29', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `future_dictionary` VALUES ('2ec7222ff4284c9b9c6a4d6eb3abc675', 'CREATE_QUERY_TYPE', 'jqcx', '精确查询', 2, NULL, '2019-12-25 21:36:50', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `future_dictionary` VALUES ('2f9d140729d34f1b9476b40cc359c325', 'CREATE_QUERY_TYPE', 'like', '模糊查询', 1, NULL, '2019-12-25 21:36:36', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `future_dictionary` VALUES ('32c4d234ee5c4705ab0a8ac33cabcb8b', 'CREATE_SHOW_FORM_TYPE', 'radio', '单选框(true/false)', 3, NULL, '2019-12-25 21:39:09', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-25 21:39:24', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2');
INSERT INTO `future_dictionary` VALUES ('355a91da663f4e4fa72387eb9e245f71', 'CREATE_SHOW_FORM_TYPE', 'date', '日期框', 2, NULL, '2019-12-25 21:38:50', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `future_dictionary` VALUES ('7010de099dcd4cf2a438438dddc2273c', 'UNIT_TYPE', 'main', '主要部门', 1, NULL, '2019-12-25 21:50:25', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `future_dictionary` VALUES ('72917cd083e74bcd89b47e8b7e05e077', 'CREATE_SHOW_FORM_TYPE', 'dictionarySelect', '下拉框(数据字典)', 4, NULL, '2019-12-25 21:40:02', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `future_dictionary` VALUES ('7b2be6336799469ab78949f012f6f9ce', 'ORG_MENU_TYPE', 'BUTTON', '资源', 2, NULL, '2020-01-02 19:25:22', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-08 11:08:30', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2');
INSERT INTO `future_dictionary` VALUES ('8817dea570e34c7b98a7fbb0f6a72ea8', 'ORG_USER_SEX', 'WOMAN', '女', 2, NULL, '2020-05-11 17:54:49', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `future_dictionary` VALUES ('b2aefb58cd8946c6bef18031f3f86e66', 'ORG_USER_SEX', 'MAN', '男', 1, NULL, '2020-05-11 17:54:31', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `future_dictionary` VALUES ('def0211d8dbb4bc58afcc8a19b779a17', 'ORG_USER_ROLE_GROUP_TYPE', 'USER', '用户', 1, NULL, '2020-01-03 09:27:28', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `future_dictionary` VALUES ('df6b3fe9798f4190b6a8cf179e6c26cd', 'ORG_USER_ROLE_GROUP_TYPE', 'DEPT', '部门', 2, NULL, '2020-01-03 09:27:42', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `future_dictionary` VALUES ('e16e41bbebd647cf90f0da7427c42422', 'CREATE_QUERY_TYPE', 'dictionarySelect', '数据字典查询', 3, NULL, '2019-12-25 21:37:14', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `future_dictionary` VALUES ('e3f6cc4105674510b034b22032a9c4de', 'UNIT_TYPE', 'special', '特殊部门', 2, NULL, '2019-12-25 21:50:56', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `future_dictionary` VALUES ('f4811b540327449dba816fbf62e40968', 'ORG_MENU_TYPE', 'MENU', '菜单', 1, NULL, '2020-01-02 19:25:07', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `future_dictionary` VALUES ('f979ae47652649e4b580a70a8caeac2c', 'CREATE_SHOW_FORM_TYPE', 'textarea', '文本框', 5, NULL, '2019-12-25 21:40:21', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);

-- ----------------------------
-- Table structure for future_dictionary_type
-- ----------------------------
DROP TABLE IF EXISTS `future_dictionary_type`;
CREATE TABLE `future_dictionary_type`  (
  `dictionaryTypeId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据字典类型id',
  `codeItemId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典类别标识',
  `codeItemName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典类别名称',
  `parentId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据字典类型父id',
  `sortNo` int(4) NULL DEFAULT NULL COMMENT '类型排序',
  `isUpdate` tinyint(1) NOT NULL COMMENT '能否被更新',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`dictionaryTypeId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_dictionary_type
-- ----------------------------
INSERT INTO `future_dictionary_type` VALUES ('52f7f3bc4cd947259ff790a8a3135f88', 'ORG_USER_SEX', '用户性别', NULL, 1002, 1, 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-11 17:54:13', NULL, NULL);
INSERT INTO `future_dictionary_type` VALUES ('70e046c8d718428590fd71449094ae9f', 'CREATE_SHOW_FORM_TYPE', '表单展现方式', NULL, 999902, 1, 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-25 21:38:10', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-25 22:21:22');
INSERT INTO `future_dictionary_type` VALUES ('92ec02aa349642478ea7f87104d65935', 'ORG_USER_ROLE_GROUP_TYPE', '角色用户关联类型', NULL, 11, 1, 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-01-03 09:27:02', NULL, NULL);
INSERT INTO `future_dictionary_type` VALUES ('dc110e626668430a90aeea8afb10be00', 'ORG_MENU_TYPE', '菜单类型', NULL, 11, 1, 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-01-02 19:24:52', NULL, NULL);
INSERT INTO `future_dictionary_type` VALUES ('e928fba1c7f94b08a77a65bff2d7cd47', 'UNIT_TYPE', '部门类型', NULL, 999911, 1, 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-25 21:50:10', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-25 22:20:58');
INSERT INTO `future_dictionary_type` VALUES ('ee24335ccc694e94839158c19094fc47', 'CREATE_QUERY_TYPE', '查询方式', NULL, 999901, 1, 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-25 21:35:47', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-25 22:21:11');

-- ----------------------------
-- Table structure for future_exception_log
-- ----------------------------
DROP TABLE IF EXISTS `future_exception_log`;
CREATE TABLE `future_exception_log`  (
  `exceptionLogId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作日志id',
  `loginUserName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录名称',
  `loginIp` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录ip',
  `loginArea` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录地址',
  `requestUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求地址',
  `requestMethod` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求方法',
  `requestData` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求数据',
  `exceptionMessage` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '异常信息',
  `exceptionData` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '异常数据',
  `browser` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '浏览器',
  `os` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作系统',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '登录日期',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`exceptionLogId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '异常日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_exception_log
-- ----------------------------
INSERT INTO `future_exception_log` VALUES ('28bd65f64d59430298982349bfe88201', '久伴轻尘呀', '127.0.0.1', '局域网', '/org/user/page', 'GET', '{\"extData\":{\"userAccount\":\"z\",\"limit\":\"10\",\"unitId\":\"1\",\"page\":\"1\"},\"rawData\":\"\"}', 'java.sql.SQLException: sql injection violation, syntax error: ERROR. token : PERCENT, pos : 103 : select count(*) FROM \n	future_user AS `future_user`\nWHERE 1=1\n	\n		AND `future_user`.userAccount  like %z%\n	\n	\n\n\n\n\n\n\n', 'com.jfinal.plugin.activerecord.ActiveRecordException: java.sql.SQLException: sql injection violation, syntax error: ERROR. token : PERCENT, pos : 103 : select count(*) FROM \n	future_user AS `future_user`\nWHERE 1=1\n	\n		AND `future_user`.userAccount  like %z%\n	\n	\n\n\n\n\n\n\n', 'Chrome 8', 'Windows 10', '2020-07-16 10:23:36', '21dbcbb7889840359426b368c74a9375');
INSERT INTO `future_exception_log` VALUES ('7d306597e78e4604a54c395da10c1cc5', '久伴轻尘呀', '127.0.0.1', '局域网', '/org/user/restPass', 'POST', '{\"extData\":{},\"rawData\":\"{\\\"oldPassword\\\":\\\"234567\\\",\\\"newPassword\\\":\\\"123456\\\"}\"}', 'java.lang.Exception: 旧密码错误', 'java.lang.RuntimeException: java.lang.Exception: 旧密码错误', 'Chrome 8', 'Windows 10', '2020-07-14 10:04:45', '21dbcbb7889840359426b368c74a9375');

-- ----------------------------
-- Table structure for future_login_log
-- ----------------------------
DROP TABLE IF EXISTS `future_login_log`;
CREATE TABLE `future_login_log`  (
  `loginLogId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录日志id',
  `loginUserName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录名称',
  `loginIp` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录ip',
  `loginArea` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录地址',
  `browser` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '浏览器',
  `os` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作系统',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '登录日期',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`loginLogId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户登录日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_login_log
-- ----------------------------

-- ----------------------------
-- Table structure for future_menu
-- ----------------------------
DROP TABLE IF EXISTS `future_menu`;
CREATE TABLE `future_menu`  (
  `menuId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单id',
  `menuName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单name',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单title',
  `subTitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '二级标题',
  `keepAlive` tinyint(1) NULL DEFAULT NULL COMMENT '是否缓存',
  `path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'path',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件地址',
  `redirect` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跳转地址',
  `hidden` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示',
  `hiddenHeaderContent` tinyint(1) NULL DEFAULT NULL COMMENT '是否显示头部信息',
  `menuType` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单类型',
  `authorityKey` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `sortNo` int(4) NULL DEFAULT NULL COMMENT '菜单排序',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `parentId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父菜单id',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`menuId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_menu
-- ----------------------------
INSERT INTO `future_menu` VALUES ('015a513f343d4bcfaaadb388331a7a25', 'OrgAuthority', '组织权限管理', NULL, 0, 'orgAuthority', 'PageView', '/org/orgAuthority/userlist', 0, 0, 'MENU', NULL, 1101, 'icon-org', '2caf91104180448b8febfbce8ed9bacd', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 11:48:52', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 14:34:54');
INSERT INTO `future_menu` VALUES ('01cc7ea7048244528a36fbfcb19ff28d', NULL, '角色新增', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:role:add', 1, NULL, '840c4d3cf0914391852651521995974f', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:09:22', NULL, NULL);
INSERT INTO `future_menu` VALUES ('06d086ecfd7b464c8fb44ff777a76a9d', NULL, '数据字典类型新增', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'cms:dictionaryType:add', 1, NULL, '12a9ea5a50ff47e090bf57c53be1cff9', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:27:02', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:31:33');
INSERT INTO `future_menu` VALUES ('08082a524a18438fa88793ad5ed8a9fc', NULL, '文件新增', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:file:add', 1110, NULL, '2caf91104180448b8febfbce8ed9bacd', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-06-30 14:39:40', NULL, NULL);
INSERT INTO `future_menu` VALUES ('0e03f763792d4462abe6ec0757da6435', NULL, '角色修改', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:role:update', 2, NULL, '840c4d3cf0914391852651521995974f', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:09:39', NULL, NULL);
INSERT INTO `future_menu` VALUES ('10dd45841946401e98b43afca328cf8f', 'AnonymousList', '匿名访问列表', NULL, 0, 'list', '/org/anonymous/list', NULL, 1, 0, 'MENU', 'org:anonymous:view', 110401, 'icon-role', '50c59a9170ff43998f8c4a4cad705b73', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-31 17:38:07', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:24:35');
INSERT INTO `future_menu` VALUES ('1132d7602f8b4eb1bfb1e0f6639b0820', 'CmsDictionaryType', '数据字典', NULL, 0, 'dictionaryType', 'PageView', '/cms/dictionaryType/list', 0, 0, 'MENU', NULL, 1201, 'icon-dictionary', '172fe21c71da42c18156619768a8b119', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 12:34:57', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 14:36:55');
INSERT INTO `future_menu` VALUES ('12a9ea5a50ff47e090bf57c53be1cff9', 'CmsDictionaryTypeList', '数据字典列表', NULL, 0, 'list', '/cms/dictionaryType/list', '', 1, 0, 'MENU', 'cms:dictionaryType:view', 120101, 'icon-dictionary', '1132d7602f8b4eb1bfb1e0f6639b0820', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 12:36:43', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:31:41');
INSERT INTO `future_menu` VALUES ('15f569f0969c455698caf1d078dd7ae5', NULL, '匿名访问删除', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:anonymous:delete', 110405, NULL, '50c59a9170ff43998f8c4a4cad705b73', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:53:59', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:59:36');
INSERT INTO `future_menu` VALUES ('172fe21c71da42c18156619768a8b119', 'Cms', '系统工具', NULL, 0, '/cms', 'BasicLayout', '/cms/dictionaryType', 0, 0, 'MENU', NULL, 12, 'icon-tool', 'jxhx', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-20 17:23:13', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 14:36:43');
INSERT INTO `future_menu` VALUES ('18a26359ccf1440bb2f23b8620fed6c8', NULL, '菜单新增', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:menu:add', 1, NULL, 'e8c9c089413746928c003947c67227dc', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:23:24', NULL, NULL);
INSERT INTO `future_menu` VALUES ('19750169980c4e72a70cf95090ee64e7', NULL, '匿名访问新增', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:anonymous:add', 110403, NULL, '50c59a9170ff43998f8c4a4cad705b73', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:52:17', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:59:19');
INSERT INTO `future_menu` VALUES ('19a90df25dda4897a7be6454b0954c6d', NULL, '定时任务修改', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'cms:cronTask:update', 2, NULL, '7127e100139b4faaa8016e512e10ada7', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:32:26', NULL, NULL);
INSERT INTO `future_menu` VALUES ('1bab54cb736e4395aeda3d9e42cc34ac', NULL, '角色资源新增', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:resourceAuthority:add', 1, NULL, 'a2e28e280578447dafcf1ae52a9a2996', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:21:22', NULL, NULL);
INSERT INTO `future_menu` VALUES ('1c2e8cab4c6e4bcd9f3754c7ee80ca20', 'Analysis', '分析页', NULL, NULL, 'analysis', '/dashboard/analysis', NULL, 0, 0, 'MENU', 'common', 1001, 'icon-fenxiye', '728acfe3729348059b3e76e77213dfe0', 'a8121140323b4200aee4630e9f191379', '2020-07-13 10:36:55', 'a8121140323b4200aee4630e9f191379', '2020-07-13 10:37:05');
INSERT INTO `future_menu` VALUES ('2caf91104180448b8febfbce8ed9bacd', 'org', '系统设置', NULL, 0, '/org', 'BasicLayout', '/org/orgAuthority', 0, 0, 'MENU', '', 11, 'icon-system', 'jxhx', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-19 11:20:43', '21dbcbb7889840359426b368c74a9375', '2020-06-05 20:51:23');
INSERT INTO `future_menu` VALUES ('2e6b6763767346f6a90f4254610777b7', NULL, '匿名访问修改', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:anonymous:update', 110404, NULL, '50c59a9170ff43998f8c4a4cad705b73', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:52:55', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:59:28');
INSERT INTO `future_menu` VALUES ('2f06eaee1ef9445a9242c157f1efef2b', 'GeneratorList', '代码列表', NULL, 0, 'list', '/cms/generator/list', NULL, 1, 0, 'MENU', 'cms:table:view', 102401, 'icon-code', '4f299e51f9934c64bc2491180ed7231e', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-23 17:52:33', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:44:10');
INSERT INTO `future_menu` VALUES ('3830575b8e4d4eee8515bd48496bc4c5', 'LoginLog', '登录日志', NULL, 0, 'loginLogList', '/org/loginLog/list', NULL, 0, 0, 'MENU', 'org:loginLog:view', 110301, 'icon-loginLog', '7d0632d0fc4f474f8d5f912171f13c2e', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-31 15:53:29', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:25:26');
INSERT INTO `future_menu` VALUES ('44014295c47c44eaabcb0c02a4c70575', 'UserInfo', '个人中心', NULL, NULL, 'userInfo', '/dashboard/userInfo', NULL, 0, 0, 'MENU', 'common', 1002, 'icon-user', '728acfe3729348059b3e76e77213dfe0', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-06-04 20:28:49', '21dbcbb7889840359426b368c74a9375', '2020-06-05 20:49:59');
INSERT INTO `future_menu` VALUES ('46fa727ecd414a16856ec3d99382a51e', NULL, '部门修改', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:unit:update', 2, NULL, '9aebaf5bbad44b8a92bf2af27444e599', '21dbcbb7889840359426b368c74a9375', '2020-06-05 20:59:24', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:00:05');
INSERT INTO `future_menu` VALUES ('4ae9d63b13964af7bc9823e76af03a0a', 'ExceptionLog', '异常日志', NULL, NULL, 'exceptionLogList', '/org/exceptionLog/list', NULL, 0, 0, 'MENU', 'org:exceptionLog:view', 110305, 'icon-loginLog', '7d0632d0fc4f474f8d5f912171f13c2e', '21dbcbb7889840359426b368c74a9375', '2020-07-13 17:39:22', '21dbcbb7889840359426b368c74a9375', '2020-07-13 17:40:33');
INSERT INTO `future_menu` VALUES ('4f299e51f9934c64bc2491180ed7231e', 'Generator', '代码生成', NULL, 0, 'generator', 'PageView', '/cms/generator/list', 0, 0, 'MENU', NULL, 1204, 'icon-code', '172fe21c71da42c18156619768a8b119', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-23 17:50:32', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-23 17:50:55');
INSERT INTO `future_menu` VALUES ('50c59a9170ff43998f8c4a4cad705b73', 'Anonymous', '匿名访问', NULL, 0, 'anonymous', 'PageView', '/org/anonymous/list', 0, 0, 'MENU', NULL, 1104, 'icon-role', '2caf91104180448b8febfbce8ed9bacd', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-31 17:37:14', NULL, NULL);
INSERT INTO `future_menu` VALUES ('5233b18204ba4b95af21cffc187af3a2', NULL, '数据字典类型修改', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'cms:dictionaryType:update', 2, NULL, '12a9ea5a50ff47e090bf57c53be1cff9', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:27:34', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:31:25');
INSERT INTO `future_menu` VALUES ('5373e71f4391484681f135c9bb28aaf2', 'CmsDictionaryTypeForm', '数据字典详情', NULL, 0, 'form/:id?', '/cms/dictionaryType/form', NULL, 1, 0, 'MENU', NULL, 120102, 'icon-dictionary', '1132d7602f8b4eb1bfb1e0f6639b0820', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 12:37:31', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 14:37:28');
INSERT INTO `future_menu` VALUES ('59165c79a5fa475282d83c0e2829f9eb', NULL, '用户部门删除', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:userDept:delete', 3, NULL, 'd97d05872ba64fb4b875ccc6545cbd93', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:06:19', NULL, NULL);
INSERT INTO `future_menu` VALUES ('59808c21098344cda98799468802f9d0', NULL, '角色用户组修改', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:userGroupRole:updadte', 2, NULL, '6bfa0e0854444cb6b4dd36c859584169', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:12:46', NULL, NULL);
INSERT INTO `future_menu` VALUES ('6bfa0e0854444cb6b4dd36c859584169', NULL, '角色用户组管理', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:userGroupRole:view', 11010401, NULL, 'ba333fa677724290862645c59cef92ae', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:11:57', NULL, NULL);
INSERT INTO `future_menu` VALUES ('6ea3cde940d74096baf4caebe3d584ee', 'OperateLog', '操作日志', NULL, NULL, 'operateLogList', '/org/operateLog/list', NULL, 0, 0, 'MENU', 'org:operateLog:view', 110303, 'icon-loginLog', '7d0632d0fc4f474f8d5f912171f13c2e', '21dbcbb7889840359426b368c74a9375', '2020-07-13 17:42:33', NULL, NULL);
INSERT INTO `future_menu` VALUES ('7127e100139b4faaa8016e512e10ada7', 'cronTaskList', '定时任务列表', NULL, 0, 'list', '/cms/cronTask/list', NULL, 1, 0, 'MENU', 'cms:cronTask:view', 120301, 'icon-yibiaopan', 'e86540538ce241feab0a65befe6ed148', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-20 20:12:43', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:30:31');
INSERT INTO `future_menu` VALUES ('728acfe3729348059b3e76e77213dfe0', 'Dashboard', '仪表盘', NULL, 0, '/dashboard', 'BasicLayout', '/dashboard/workplace', 0, 0, 'MENU', NULL, 10, 'icon-yibiaopan', 'jxhx', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 13:47:58', '21dbcbb7889840359426b368c74a9375', '2020-06-08 19:23:42');
INSERT INTO `future_menu` VALUES ('7491ac26988e40ad9f289e1c77e41134', NULL, '部门删除', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:unit:delete', 3, NULL, '9aebaf5bbad44b8a92bf2af27444e599', '21dbcbb7889840359426b368c74a9375', '2020-06-05 20:59:59', NULL, NULL);
INSERT INTO `future_menu` VALUES ('74d9270b11e547429caf598465b3ff72', 'OrgUserForm', '用户详情', NULL, 0, 'userForm/:id?', '/org/user/form', NULL, 1, 0, 'MENU', NULL, 110102, 'icon-user', '015a513f343d4bcfaaadb388331a7a25', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 11:56:04', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 14:35:52');
INSERT INTO `future_menu` VALUES ('7636ec9f61b84a7a8eb82bcbc6cc7e57', NULL, '菜单修改', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:menu:update', 2, NULL, 'e8c9c089413746928c003947c67227dc', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:23:46', NULL, NULL);
INSERT INTO `future_menu` VALUES ('7d0632d0fc4f474f8d5f912171f13c2e', 'LogManager', '日志管理', NULL, 0, 'logManager', 'PageView', '/org/logManager/loginLog', 0, 0, 'MENU', NULL, 1108, 'icon-operateLog', '2caf91104180448b8febfbce8ed9bacd', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-31 15:52:06', '21dbcbb7889840359426b368c74a9375', '2020-06-05 20:44:18');
INSERT INTO `future_menu` VALUES ('840c4d3cf0914391852651521995974f', 'OrgRoleList', '角色与权限', NULL, 0, 'roleList', '/org/role/list', NULL, 0, 0, 'MENU', 'org:role:view', 110103, 'icon-role', '015a513f343d4bcfaaadb388331a7a25', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 11:57:23', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:08:02');
INSERT INTO `future_menu` VALUES ('852d065e30364f64a9b265e69b16c7da', 'OrgMenu', '菜单管理', NULL, 0, 'orgMenu', 'PageView', '/org/orgMenu/menuList', 0, 0, 'MENU', NULL, 1102, 'icon-menu', '2caf91104180448b8febfbce8ed9bacd', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 11:59:31', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 14:35:19');
INSERT INTO `future_menu` VALUES ('8552ae310714404c833b143807ad1203', NULL, '模块管理', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:module:view', 12040201, NULL, 'f490c0acf7fd446eb729f9f60b148895', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:47:26', NULL, NULL);
INSERT INTO `future_menu` VALUES ('890da1b364af45f697ac31ce83a1299f', 'AnonymousForm', '匿名访问详情', NULL, 0, 'form/:id?', '/org/anonymous/form', NULL, 1, 0, 'MENU', NULL, 110402, 'icon-role', '50c59a9170ff43998f8c4a4cad705b73', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-31 17:39:36', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-31 17:40:29');
INSERT INTO `future_menu` VALUES ('8a4774c615bc4deb9c60ef4b7ec1ab8b', NULL, '定时任务新增', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'cms:cronTask:add', 1, NULL, '7127e100139b4faaa8016e512e10ada7', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:32:08', NULL, NULL);
INSERT INTO `future_menu` VALUES ('8a82b821642c43308b5a8b76d033b399', 'CmsIconList', '图标管理', NULL, 0, 'list', '/cms/icon', NULL, 1, 0, 'MENU', NULL, 120201, 'icon-icons', 'da8ef2410cce47ca9864125db2ad6265', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 12:39:29', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 14:37:58');
INSERT INTO `future_menu` VALUES ('8c097617924145bba2e5f4a853e06d7e', NULL, '用户导出', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:user:export', 110105, NULL, 'b1eecda9d7f84e4f923ecbfe647f4bb9', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-06-30 09:15:00', NULL, NULL);
INSERT INTO `future_menu` VALUES ('93a06c3af1f94d24a1cfd6978458e7f0', NULL, '用户部门新增', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:userDept:add', 1, NULL, 'd97d05872ba64fb4b875ccc6545cbd93', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:05:43', NULL, NULL);
INSERT INTO `future_menu` VALUES ('98734957d1914c449dcbf95bf418efb7', NULL, '菜单删除', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:menu:delete', 3, NULL, 'e8c9c089413746928c003947c67227dc', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:24:04', NULL, NULL);
INSERT INTO `future_menu` VALUES ('9aebaf5bbad44b8a92bf2af27444e599', NULL, '部门管理', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:unit:view', 11010101, NULL, 'fbd2e1a5bfec4fabb29f68ca184bf7a8', '21dbcbb7889840359426b368c74a9375', '2020-06-05 20:57:03', NULL, NULL);
INSERT INTO `future_menu` VALUES ('9c0bda7ab1a043e6809891bae27e594f', NULL, '角色用户组删除', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:userGroupRole:delete', 3, NULL, '6bfa0e0854444cb6b4dd36c859584169', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:13:09', NULL, NULL);
INSERT INTO `future_menu` VALUES ('9fc5a2c5a5054f9591396afe546d3a31', NULL, '模块新增', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:module:add', 1, NULL, '8552ae310714404c833b143807ad1203', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:48:01', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:48:40');
INSERT INTO `future_menu` VALUES ('a2e28e280578447dafcf1ae52a9a2996', NULL, '角色资源管理', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:resourceAuthority:view', 11010402, NULL, 'ba333fa677724290862645c59cef92ae', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:20:56', NULL, NULL);
INSERT INTO `future_menu` VALUES ('a408767e5ace4d7bbcd3f1022f716b0d', 'LoginLogForm', '登录日志详情', NULL, 0, 'loginLogForm/:id?', '/org/loginLog/form', NULL, 1, 0, 'MENU', NULL, 110302, 'icon-loginLog', '7d0632d0fc4f474f8d5f912171f13c2e', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-31 16:04:54', NULL, NULL);
INSERT INTO `future_menu` VALUES ('a66aacb9d62247fea49baed967924618', NULL, '用户修改', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:user:update', 2, NULL, 'b1eecda9d7f84e4f923ecbfe647f4bb9', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:01:29', NULL, NULL);
INSERT INTO `future_menu` VALUES ('a7057775e3834e66bb33369bf187476c', NULL, '代码生成', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'cms:table:add', 120401, NULL, '2f06eaee1ef9445a9242c157f1efef2b', '21dbcbb7889840359426b368c74a9375', '2020-07-05 17:53:58', NULL, NULL);
INSERT INTO `future_menu` VALUES ('a7b69b4c81714e5088652e19c0bb6b1f', NULL, '角色资源修改', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:resourceAuthority:update', 2, NULL, 'a2e28e280578447dafcf1ae52a9a2996', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:21:43', NULL, NULL);
INSERT INTO `future_menu` VALUES ('b0795292364241889c100833c7a5e334', NULL, '角色删除', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:role:delete', 3, NULL, '840c4d3cf0914391852651521995974f', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:10:03', NULL, NULL);
INSERT INTO `future_menu` VALUES ('b1774ab17f354ba3b16a5844c12522c9', NULL, '角色资源删除', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:resourceAuthority:delete', 3, NULL, 'a2e28e280578447dafcf1ae52a9a2996', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:22:08', NULL, NULL);
INSERT INTO `future_menu` VALUES ('b1eecda9d7f84e4f923ecbfe647f4bb9', NULL, '用户管理', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:user:view', 11010102, NULL, 'fbd2e1a5bfec4fabb29f68ca184bf7a8', '21dbcbb7889840359426b368c74a9375', '2020-06-05 20:57:38', NULL, NULL);
INSERT INTO `future_menu` VALUES ('b2ea95d3296047949a292fabea2725c1', NULL, '数据字典类型删除', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'cms:dictionaryType:delete', 3, NULL, '12a9ea5a50ff47e090bf57c53be1cff9', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:27:55', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:31:17');
INSERT INTO `future_menu` VALUES ('ba333fa677724290862645c59cef92ae', 'OrgRoleForm', '角色详情', NULL, 0, 'roleForm/:id?', '/org/role/form', NULL, 1, 1, 'MENU', NULL, 110104, 'icon-role', '015a513f343d4bcfaaadb388331a7a25', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 11:58:16', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 14:36:11');
INSERT INTO `future_menu` VALUES ('bbd56e8098d94bfe99a4863cf938fd58', NULL, '数据字典子项修改', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'cms:dictionary:update', 2, NULL, 'f943f204c09048a289d523da767ce5bf', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:29:35', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:30:51');
INSERT INTO `future_menu` VALUES ('c5636988726c45e0a9f8e42f5ea35426', NULL, '用户删除', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:user:delete', 3, NULL, 'b1eecda9d7f84e4f923ecbfe647f4bb9', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:02:09', NULL, NULL);
INSERT INTO `future_menu` VALUES ('c67c612cf2de425595b3ebc312aecb62', 'Workplace', '工作台', NULL, NULL, 'workplace', '/dashboard/workplace', NULL, 0, 0, 'MENU', 'common', 1003, 'icon-fenxiye', '728acfe3729348059b3e76e77213dfe0', '21dbcbb7889840359426b368c74a9375', '2020-06-07 15:43:58', '21dbcbb7889840359426b368c74a9375', '2020-06-07 15:44:22');
INSERT INTO `future_menu` VALUES ('ceffd9c2458a409ca3d00c31e9fe17c8', NULL, '模块删除', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:module:delete', 3, NULL, '8552ae310714404c833b143807ad1203', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:48:34', NULL, NULL);
INSERT INTO `future_menu` VALUES ('cf9219edb060458f858f3898587a2f43', NULL, '数据字典子项删除', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'cms:dictionary:delete', 3, NULL, 'f943f204c09048a289d523da767ce5bf', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:29:58', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:30:45');
INSERT INTO `future_menu` VALUES ('d2f6cd36120d437191bc9ac8c2d484b1', NULL, '用户部门修改', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:userDept:update', 2, NULL, 'd97d05872ba64fb4b875ccc6545cbd93', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:06:03', NULL, NULL);
INSERT INTO `future_menu` VALUES ('d97d05872ba64fb4b875ccc6545cbd93', NULL, '用户部门管理', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:userDept:view', 11010201, NULL, '74d9270b11e547429caf598465b3ff72', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:05:21', NULL, NULL);
INSERT INTO `future_menu` VALUES ('da8ef2410cce47ca9864125db2ad6265', 'CmsIcon', '图标管理', NULL, 0, 'icon', 'PageView', '/cms/icon/list', 0, 0, 'MENU', NULL, 1202, 'icon-icons', '172fe21c71da42c18156619768a8b119', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 12:38:49', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 14:37:05');
INSERT INTO `future_menu` VALUES ('dad7c70621e9472e8cf1a9faae840dab', NULL, '定时任务删除', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'cms:cronTask:delete', 3, NULL, '7127e100139b4faaa8016e512e10ada7', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:34:37', NULL, NULL);
INSERT INTO `future_menu` VALUES ('dd43efb1c148469bae3c51161527314e', NULL, '模块修改', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:module:update', 2, NULL, '8552ae310714404c833b143807ad1203', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:48:19', NULL, NULL);
INSERT INTO `future_menu` VALUES ('e46502e8146d44e48e80d9976badf2b2', NULL, '部门新增', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:unit:add', 1, NULL, '9aebaf5bbad44b8a92bf2af27444e599', '21dbcbb7889840359426b368c74a9375', '2020-06-05 20:58:36', NULL, NULL);
INSERT INTO `future_menu` VALUES ('e86540538ce241feab0a65befe6ed148', 'cronTask', '定时任务', NULL, 0, 'cronTask', 'PageView', '/cms/cronTask/list', 0, 0, 'MENU', NULL, 1203, 'icon-fujian', '172fe21c71da42c18156619768a8b119', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-20 20:11:32', NULL, NULL);
INSERT INTO `future_menu` VALUES ('e8c9c089413746928c003947c67227dc', 'OrgMenuList', '菜单列表', '这是菜单列表', 0, 'menuList', '/org/menu/list', NULL, 1, 0, 'MENU', 'org:menu:view', 110201, 'icon-menu', '852d065e30364f64a9b265e69b16c7da', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 12:00:43', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:22:38');
INSERT INTO `future_menu` VALUES ('e9848e49efbb4de29b6e35eec7ff3111', NULL, '角色用户组新增', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:userGroupRole:add', 1, NULL, '6bfa0e0854444cb6b4dd36c859584169', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:12:26', NULL, NULL);
INSERT INTO `future_menu` VALUES ('f25005bd9f9e430ba94d451a34419077', NULL, '用户新增', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'org:user:add', 1, NULL, 'b1eecda9d7f84e4f923ecbfe647f4bb9', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:00:57', NULL, NULL);
INSERT INTO `future_menu` VALUES ('f490c0acf7fd446eb729f9f60b148895', 'GeneratorForm', '代码生成详情', NULL, 0, 'form', '/cms/generator/form', NULL, 1, 1, 'MENU', NULL, 120402, 'icon-code', '4f299e51f9934c64bc2491180ed7231e', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-23 18:10:33', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-31 11:15:37');
INSERT INTO `future_menu` VALUES ('f943f204c09048a289d523da767ce5bf', NULL, '数据字典子项', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'cms:dictionary:view', 12010201, NULL, '5373e71f4391484681f135c9bb28aaf2', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:28:41', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:31:05');
INSERT INTO `future_menu` VALUES ('fbd2e1a5bfec4fabb29f68ca184bf7a8', 'OrgUserList', '组织与用户', '这是一个包含了部门管理与用户管理的组合页面', 1, 'userList', '/org/user/list', '', 0, 0, 'MENU', '', 110101, 'icon-user', '015a513f343d4bcfaaadb388331a7a25', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-17 11:54:41', '21dbcbb7889840359426b368c74a9375', '2020-06-05 20:53:44');
INSERT INTO `future_menu` VALUES ('fd42b2155e5a4dff85cae8d4249b129f', NULL, '数据字典子项新增', NULL, NULL, NULL, NULL, NULL, 0, 0, 'BUTTON', 'cms:dictionary:add', 1, NULL, 'f943f204c09048a289d523da767ce5bf', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:29:05', '21dbcbb7889840359426b368c74a9375', '2020-06-05 21:30:58');

-- ----------------------------
-- Table structure for future_module
-- ----------------------------
DROP TABLE IF EXISTS `future_module`;
CREATE TABLE `future_module`  (
  `moduleId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模块id',
  `moduleName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块描述',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块路径',
  `package` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块包名',
  `sortNo` int(4) NULL DEFAULT NULL COMMENT '模块排序',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`moduleId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '模块表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_module
-- ----------------------------
INSERT INTO `future_module` VALUES ('1', 'org', '系统组织模块', '/future-org', 'com.qc.future.org', 1, NULL, NULL, NULL, NULL);
INSERT INTO `future_module` VALUES ('2a40cc0c471246c2bedee72319714bcf', 'cms', '系统工具模块', '/future-cms', 'com.qc.future.cms', 2, 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-25 19:47:33', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-31 13:16:51');

-- ----------------------------
-- Table structure for future_operate_log
-- ----------------------------
DROP TABLE IF EXISTS `future_operate_log`;
CREATE TABLE `future_operate_log`  (
  `operateLogId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作日志id',
  `loginUserName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录名称',
  `loginIp` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录ip',
  `loginArea` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录地址',
  `requestUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求地址',
  `requestMethod` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求方法',
  `requestData` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求数据',
  `browser` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '浏览器',
  `os` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作系统',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '登录日期',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`operateLogId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_operate_log
-- ----------------------------

-- ----------------------------
-- Table structure for future_resource_authority
-- ----------------------------
DROP TABLE IF EXISTS `future_resource_authority`;
CREATE TABLE `future_resource_authority`  (
  `resourceAuthorityId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源权限id',
  `roleId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id',
  `resourceId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源id',
  `resourceType` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源类型',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`resourceAuthorityId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色资源关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_resource_authority
-- ----------------------------
INSERT INTO `future_resource_authority` VALUES ('00e474e5c3c24d58b29a0ba0d199db93', 'jiubanqingchen:admin', 'b0795292364241889c100833c7a5e334', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('04f3982e665f4b5db0b722a1da484ad7', 'e55c4dae7ab941619052c5a6d9067b1e', '10dd45841946401e98b43afca328cf8f', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('05a955e2bca74be7a99174dab36c6081', 'jiubanqingchen:admin', 'fd42b2155e5a4dff85cae8d4249b129f', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('05de241acee4448dab4552e27f1e5d48', 'e55c4dae7ab941619052c5a6d9067b1e', '4f299e51f9934c64bc2491180ed7231e', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('0837a9bded8f4fdcafeba50ca14e6602', 'e55c4dae7ab941619052c5a6d9067b1e', '6bfa0e0854444cb6b4dd36c859584169', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('08600d9933b044e5befe689dda091c00', 'jiubanqingchen:admin', '9fc5a2c5a5054f9591396afe546d3a31', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('086e14edd2144715a9d855230c255391', 'jiubanqingchen:admin', '8a4774c615bc4deb9c60ef4b7ec1ab8b', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('0a4c56e890bc450086ee3274a07dbfbe', 'jiubanqingchen:admin', '19750169980c4e72a70cf95090ee64e7', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:14', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('0d046ff118a34dc680db151f6e06b7f1', 'jiubanqingchen:admin', 'fbd2e1a5bfec4fabb29f68ca184bf7a8', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('13c598ce3dfe4c4a83500b5bfa45cf43', 'e55c4dae7ab941619052c5a6d9067b1e', 'c67c612cf2de425595b3ebc312aecb62', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('1883e81b6da241d29ae1ba0aee7e342a', 'jiubanqingchen:admin', '59165c79a5fa475282d83c0e2829f9eb', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('1dca810154464374b14a2fefdd6dfb23', 'e55c4dae7ab941619052c5a6d9067b1e', 'b1eecda9d7f84e4f923ecbfe647f4bb9', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('2039b1cbf3484e13b40390829575044a', 'e55c4dae7ab941619052c5a6d9067b1e', '1132d7602f8b4eb1bfb1e0f6639b0820', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('226f60408fc044a78d106fd99ea8020d', 'jiubanqingchen:admin', '74d9270b11e547429caf598465b3ff72', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('231078c991d0456ba9d16b1c9e64eb46', 'jiubanqingchen:admin', 'b1774ab17f354ba3b16a5844c12522c9', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('24f55fdfa87447e49b3b0607f10db1cb', 'jiubanqingchen:admin', 'ceffd9c2458a409ca3d00c31e9fe17c8', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('25f994578286462a9df632f4116c6550', 'jiubanqingchen:admin', '0e03f763792d4462abe6ec0757da6435', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:14', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('2a04f47915a84567a771898519b9a46c', 'jiubanqingchen:admin', '015a513f343d4bcfaaadb388331a7a25', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('3117673b5e5e45d6a3f40c45ed4286be', 'jiubanqingchen:admin', '6ea3cde940d74096baf4caebe3d584ee', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('32dcbe7be7834919801f4c9c266f1635', 'jiubanqingchen:admin', 'bbd56e8098d94bfe99a4863cf938fd58', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('3352351d92334e6a81f7e2aebc74ea37', 'jiubanqingchen:admin', '46fa727ecd414a16856ec3d99382a51e', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('3b6f530f9040451ab522b7053ceee741', 'e55c4dae7ab941619052c5a6d9067b1e', 'da8ef2410cce47ca9864125db2ad6265', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('3b80ca0118804fdcb5da8f09fd3a3c0d', 'jiubanqingchen:admin', '06d086ecfd7b464c8fb44ff777a76a9d', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('3cebae113f1d42d3a6a9fa98f2e02771', 'jiubanqingchen:admin', 'd97d05872ba64fb4b875ccc6545cbd93', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:14', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('3dbe34bb29154155bd47a627a493464e', 'jiubanqingchen:admin', 'c5636988726c45e0a9f8e42f5ea35426', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('3e423b1f155045a99a43f2a5d8be3e03', 'jiubanqingchen:admin', '10dd45841946401e98b43afca328cf8f', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('3ee663c96d9b4889a370cf07ebc75c86', 'jiubanqingchen:admin', '5373e71f4391484681f135c9bb28aaf2', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('41a05219b8f44522a459a561b4564ca4', 'jiubanqingchen:admin', 'e46502e8146d44e48e80d9976badf2b2', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('55108a252c924bbcb34795457a57e5d1', 'e55c4dae7ab941619052c5a6d9067b1e', 'e86540538ce241feab0a65befe6ed148', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('599284ba26f340bd820119a6670ab746', 'jiubanqingchen:admin', '172fe21c71da42c18156619768a8b119', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('5a32277c6c824fe099a73a9eb251d24d', 'e55c4dae7ab941619052c5a6d9067b1e', '890da1b364af45f697ac31ce83a1299f', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('5ca6beeb351d47a6a8acdc2d915cb6f2', 'jiubanqingchen:admin', '852d065e30364f64a9b265e69b16c7da', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('60fbc5a1551940db97298bfaf97d7498', 'e55c4dae7ab941619052c5a6d9067b1e', '7d0632d0fc4f474f8d5f912171f13c2e', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('617e892817bf4f5b91db7bc5887b4356', 'jiubanqingchen:admin', '2caf91104180448b8febfbce8ed9bacd', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:14', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('62202a6d658c4c2694d5423d6f5a530f', 'e55c4dae7ab941619052c5a6d9067b1e', '015a513f343d4bcfaaadb388331a7a25', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('6291090ca24845109dc2dcf286fe2f1f', 'e55c4dae7ab941619052c5a6d9067b1e', '50c59a9170ff43998f8c4a4cad705b73', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('66512ba2253842389862750612886f28', 'jiubanqingchen:admin', 'cf9219edb060458f858f3898587a2f43', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('6ca041412c9b41a0ac76535d638ab889', 'jiubanqingchen:admin', 'e86540538ce241feab0a65befe6ed148', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('6e16239243f54a3eb3756c62494f6ff4', 'e55c4dae7ab941619052c5a6d9067b1e', '9aebaf5bbad44b8a92bf2af27444e599', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('70f21a6816b4418eaa5ec58685590406', 'jiubanqingchen:admin', '728acfe3729348059b3e76e77213dfe0', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('73006f79ddcf4f629adf32d0f2ca7aff', 'jiubanqingchen:admin', '4f299e51f9934c64bc2491180ed7231e', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('73877e2c042044c7b00aa1a4c5898d4a', 'jiubanqingchen:admin', '890da1b364af45f697ac31ce83a1299f', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('75ed1df989fe46398c0aaa4f3f33e18c', 'jiubanqingchen:admin', 'e8c9c089413746928c003947c67227dc', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('781bcb09b96a4d7b9f6b84b6d3e3b96f', 'e55c4dae7ab941619052c5a6d9067b1e', '3830575b8e4d4eee8515bd48496bc4c5', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('7acfbf71dc2e4169a95c225de0be1a04', 'jiubanqingchen:admin', '3830575b8e4d4eee8515bd48496bc4c5', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('7de0939346814ee4ab28e5e12e29347e', 'jiubanqingchen:admin', '7d0632d0fc4f474f8d5f912171f13c2e', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('7e3a24738eca404796a8f7b648ba20c4', 'jiubanqingchen:admin', '01cc7ea7048244528a36fbfcb19ff28d', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('7f0d30786cd846bdab9077f731c3fa38', 'jiubanqingchen:admin', '840c4d3cf0914391852651521995974f', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('7fb72991a33a4687b50ef7799a8b610c', 'e55c4dae7ab941619052c5a6d9067b1e', '2f06eaee1ef9445a9242c157f1efef2b', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('800240cab318434ca2f9d9133c03ab85', 'e55c4dae7ab941619052c5a6d9067b1e', '1c2e8cab4c6e4bcd9f3754c7ee80ca20', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('8310c973036947fda876d0e146f5af4a', 'jiubanqingchen:admin', '6bfa0e0854444cb6b4dd36c859584169', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('83e82d00baf1495f856b0690e4f4a29f', 'e55c4dae7ab941619052c5a6d9067b1e', '8a82b821642c43308b5a8b76d033b399', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('84cb057aab894bdd84ce71a48769678f', 'e55c4dae7ab941619052c5a6d9067b1e', 'ba333fa677724290862645c59cef92ae', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('85fa0e0762db4906a8b9fa0b7a316c65', 'jiubanqingchen:admin', 'f25005bd9f9e430ba94d451a34419077', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('864fdeef485c45e68d3247c200a712d8', 'jiubanqingchen:admin', '8552ae310714404c833b143807ad1203', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('88a1a38a4db04b519e86e83a627f5a8d', 'e55c4dae7ab941619052c5a6d9067b1e', 'f943f204c09048a289d523da767ce5bf', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('89bb7084534a42bda38b364aa8d36996', 'jiubanqingchen:admin', '93a06c3af1f94d24a1cfd6978458e7f0', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('8bed0342bf0b4b7e805b7b88d8ccb447', 'e55c4dae7ab941619052c5a6d9067b1e', '7127e100139b4faaa8016e512e10ada7', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('8e1e62fc10ca4fa7b244295db43c81b3', 'e55c4dae7ab941619052c5a6d9067b1e', 'a408767e5ace4d7bbcd3f1022f716b0d', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('91c5e7b2ebcf4368b8ddfdb34f4c3d51', 'e55c4dae7ab941619052c5a6d9067b1e', '172fe21c71da42c18156619768a8b119', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('92e8962aeec14b47bede88bbd5289a82', 'jiubanqingchen:admin', '59808c21098344cda98799468802f9d0', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('95c16ea582344e6bbf8f0ac9ca610c64', 'jiubanqingchen:admin', '9aebaf5bbad44b8a92bf2af27444e599', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('9935d748ac304c77b20ec77311f06f67', 'jiubanqingchen:admin', '1c2e8cab4c6e4bcd9f3754c7ee80ca20', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('99e8f17889644a92a719acb2097ea042', 'jiubanqingchen:admin', 'a7b69b4c81714e5088652e19c0bb6b1f', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('9c1ef65db2e24bb8a4c2a47dfca64d9f', 'jiubanqingchen:admin', 'dd43efb1c148469bae3c51161527314e', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('9df97cabe41b4aa7ad415abcc91168db', 'jiubanqingchen:admin', '8c097617924145bba2e5f4a853e06d7e', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('9e50a7cab351466b885a6e1cb54e2d43', 'jiubanqingchen:admin', 'd2f6cd36120d437191bc9ac8c2d484b1', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('a52fcabe1b8949c682b75967481edc34', 'e55c4dae7ab941619052c5a6d9067b1e', '6ea3cde940d74096baf4caebe3d584ee', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('a56b50d1df044442ad77a00a9cc2291a', 'jiubanqingchen:admin', '1132d7602f8b4eb1bfb1e0f6639b0820', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('a71c941102df499692d08be6dd1bb249', 'e55c4dae7ab941619052c5a6d9067b1e', '74d9270b11e547429caf598465b3ff72', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('a89122813c514d28a8b5aa4df1994ac8', 'e55c4dae7ab941619052c5a6d9067b1e', '08082a524a18438fa88793ad5ed8a9fc', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('ab785f34adf74590897c50b79607b4e2', 'jiubanqingchen:admin', '2e6b6763767346f6a90f4254610777b7', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:14', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('ad96f7527bc64ca7b820b6fc2f1183b3', 'jiubanqingchen:admin', 'e9848e49efbb4de29b6e35eec7ff3111', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('aef1aa8122d847aeb3e8aade8890f795', 'e55c4dae7ab941619052c5a6d9067b1e', 'fbd2e1a5bfec4fabb29f68ca184bf7a8', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('afdd0eeafe104d70a1e4dc9a43cd9d9f', 'jiubanqingchen:admin', 'a66aacb9d62247fea49baed967924618', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('b085fe6144794ec3bc9d40cdb00610bc', 'jiubanqingchen:admin', '8a82b821642c43308b5a8b76d033b399', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('b3b89618f3be4d3dbd846d2cd1c836be', 'jiubanqingchen:admin', '1bab54cb736e4395aeda3d9e42cc34ac', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('b6893eb669ac408891f947fee884526e', 'jiubanqingchen:admin', 'a2e28e280578447dafcf1ae52a9a2996', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('b73d9a56e1644405b87d5b72699e2b30', 'jiubanqingchen:admin', '50c59a9170ff43998f8c4a4cad705b73', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('bf3bce77a08b495e8c5cfdc5b93e65d8', 'jiubanqingchen:admin', '4ae9d63b13964af7bc9823e76af03a0a', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('c2111374b25440eaa8b5b06de09fbaf2', 'e55c4dae7ab941619052c5a6d9067b1e', '8552ae310714404c833b143807ad1203', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('c28505289f1f40458aefe8db88bde40e', 'jiubanqingchen:admin', '7636ec9f61b84a7a8eb82bcbc6cc7e57', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('c3046b5c9eff46408b83026a101d3e53', 'jiubanqingchen:admin', '2f06eaee1ef9445a9242c157f1efef2b', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('c3f6f970095347a09229a042e2685d69', 'e55c4dae7ab941619052c5a6d9067b1e', '852d065e30364f64a9b265e69b16c7da', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('c5a7f72773fe40f09a82dcfa549e0159', 'jiubanqingchen:admin', 'f490c0acf7fd446eb729f9f60b148895', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('ca097c8c6a4a4b33bc39d0ffc954f409', 'e55c4dae7ab941619052c5a6d9067b1e', '4ae9d63b13964af7bc9823e76af03a0a', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('ca23071a52594c438729cc61fc459780', 'jiubanqingchen:admin', '12a9ea5a50ff47e090bf57c53be1cff9', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('caf6c655882347c0b4b3e61adbde1c91', 'jiubanqingchen:admin', '15f569f0969c455698caf1d078dd7ae5', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:14', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('cafb5fa472a5454689525d4fd86f921f', 'e55c4dae7ab941619052c5a6d9067b1e', '12a9ea5a50ff47e090bf57c53be1cff9', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('cc8b7c1fbbaa4d5ebb9f41ccc3580a55', 'e55c4dae7ab941619052c5a6d9067b1e', 'd97d05872ba64fb4b875ccc6545cbd93', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('ce8b08097f0e4121a3d8910e44a9dd82', 'jiubanqingchen:admin', 'a7057775e3834e66bb33369bf187476c', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('cee026f84d4443ab899f658d93ee3cdf', 'jiubanqingchen:admin', 'dad7c70621e9472e8cf1a9faae840dab', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('cf2a9fcde597452a91d7835bb9a2d6ef', 'jiubanqingchen:admin', '98734957d1914c449dcbf95bf418efb7', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:10', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('d4440df93fb04f99bf06c85e012a1615', 'jiubanqingchen:admin', 'a408767e5ace4d7bbcd3f1022f716b0d', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('d8cac78f6ab146d482f8ba723de53198', 'e55c4dae7ab941619052c5a6d9067b1e', '5373e71f4391484681f135c9bb28aaf2', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('d9cf7f5f510e4a11a4ff8f042e26975a', 'e55c4dae7ab941619052c5a6d9067b1e', '840c4d3cf0914391852651521995974f', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('db82fc2d208248c8931b06c90aef794c', 'jiubanqingchen:admin', '18a26359ccf1440bb2f23b8620fed6c8', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('dc08a607ab4b4ce99a05dcd37292239e', 'jiubanqingchen:admin', 'b2ea95d3296047949a292fabea2725c1', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('dcfa4b9add334706bfe9c38283926a2a', 'jiubanqingchen:admin', 'da8ef2410cce47ca9864125db2ad6265', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('dd25259d014b4e96bd3204c42405969a', 'jiubanqingchen:admin', '08082a524a18438fa88793ad5ed8a9fc', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('ddaf8cd52b254d53a5405c09456f384c', 'jiubanqingchen:admin', 'f943f204c09048a289d523da767ce5bf', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('df7d427d5e744c97aaa409ccb5735ffc', 'jiubanqingchen:admin', '19a90df25dda4897a7be6454b0954c6d', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('e1bd78a9a8cf4914ba8a094f0c964a68', 'jiubanqingchen:admin', 'ba333fa677724290862645c59cef92ae', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('e234a937e2464d97a6cd04cd81fcf4b1', 'jiubanqingchen:admin', '44014295c47c44eaabcb0c02a4c70575', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:14', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('e280053b1e0d4d419159877b9b119e28', 'jiubanqingchen:admin', '5233b18204ba4b95af21cffc187af3a2', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('e40790f6de8b4275beb778c4c14ef6d5', 'e55c4dae7ab941619052c5a6d9067b1e', 'f490c0acf7fd446eb729f9f60b148895', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('e6029879316c4436b3bd928ebb4942d2', 'e55c4dae7ab941619052c5a6d9067b1e', 'e8c9c089413746928c003947c67227dc', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('e9b3f398f5ea463f9bdea3dcf7c717b7', 'jiubanqingchen:admin', 'c67c612cf2de425595b3ebc312aecb62', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:13', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('ea96c9a596294d8bbad455a59b3d569b', 'e55c4dae7ab941619052c5a6d9067b1e', '728acfe3729348059b3e76e77213dfe0', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('f157ae52e07d4b3a882da86135093043', 'e55c4dae7ab941619052c5a6d9067b1e', 'a2e28e280578447dafcf1ae52a9a2996', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('f175335f8de74c88822f98a8535dca63', 'e55c4dae7ab941619052c5a6d9067b1e', '2caf91104180448b8febfbce8ed9bacd', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('f2bd599b169046baaf43ea706e0944c7', 'jiubanqingchen:admin', '7127e100139b4faaa8016e512e10ada7', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('f6d67c4a277744509c01593991ffac40', 'jiubanqingchen:admin', '7491ac26988e40ad9f289e1c77e41134', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('f71168987d8e4376bf471ebd4a4ff671', 'jiubanqingchen:admin', '9c0bda7ab1a043e6809891bae27e594f', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:11', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('f9c1bb221db14ef5957ab8ba5b39730d', 'e55c4dae7ab941619052c5a6d9067b1e', '44014295c47c44eaabcb0c02a4c70575', 'MENU', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-14 16:57:32', NULL, NULL);
INSERT INTO `future_resource_authority` VALUES ('fc49067d218b430d836e4eaaf1388044', 'jiubanqingchen:admin', 'b1eecda9d7f84e4f923ecbfe647f4bb9', 'MENU', '21dbcbb7889840359426b368c74a9375', '2020-07-14 10:58:12', NULL, NULL);

-- ----------------------------
-- Table structure for future_role
-- ----------------------------
DROP TABLE IF EXISTS `future_role`;
CREATE TABLE `future_role`  (
  `roleId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id',
  `roleName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `roleDescription` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `isUpdate` tinyint(1) NULL DEFAULT NULL COMMENT '能否修改',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `updateUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`roleId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_role
-- ----------------------------
INSERT INTO `future_role` VALUES ('e55c4dae7ab941619052c5a6d9067b1e', '演示角色', '演示角色', 1, '2020-07-13 10:13:50', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `future_role` VALUES ('jiubanqingchen:admin', '超级管理员', '今夕何夕超级管理员', 1, '2019-12-30 19:52:19', NULL, '2020-01-09 21:08:41', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2');

-- ----------------------------
-- Table structure for future_unit
-- ----------------------------
DROP TABLE IF EXISTS `future_unit`;
CREATE TABLE `future_unit`  (
  `unitId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门id',
  `unitCode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门编码',
  `unitName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门名称',
  `unitFullName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门全称',
  `unitType` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门类型',
  `parentId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父部门id',
  `sortNo` int(4) NULL DEFAULT NULL COMMENT '排序',
  `valid` tinyint(1) NULL DEFAULT NULL COMMENT '是否有效',
  `enabledTime` datetime(0) NULL DEFAULT NULL COMMENT '启用时间',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`unitId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_unit
-- ----------------------------
INSERT INTO `future_unit` VALUES ('1', 'QC', '轻尘科技', '轻尘科技', 'main', '0', 1, 1, '2019-12-02 00:00:00', NULL, NULL, '2020-04-30 15:45:52', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2');
INSERT INTO `future_unit` VALUES ('12', 'QC_JS_002', '技术一部', '轻尘科技/技术一部', 'main', '1', 102, 1, '2019-12-27 00:00:00', '2019-12-27 11:24:41', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-01-08 15:39:53', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2');
INSERT INTO `future_unit` VALUES ('19ca54e1024f42cf8d6fd7e6faa83125', 'QC_JS_001_YF', '研发部', '轻尘科技/技术一部/研发部', 'main', '11', 10101, 1, '2019-12-27 00:00:00', '2019-12-27 11:46:32', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2019-12-30 15:10:38', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2');
INSERT INTO `future_unit` VALUES ('1e6d6fe4edbd41f788da5975dd89c4e4', 'QC_CW_001', '财务部', '轻尘科技/财务部', 'special', '1', 103, 1, '2019-12-27 00:00:00', '2019-12-27 11:32:29', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-05-02 22:06:20', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2');
INSERT INTO `future_unit` VALUES ('544e4a2a0763404389438d0815a0d8ef', 'cw-00001', '财务一部', '轻尘科技/财务部/财务一部', 'main', '1e6d6fe4edbd41f788da5975dd89c4e4', 12, NULL, NULL, '2020-04-30 15:50:06', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-04-30 15:50:46', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2');
INSERT INTO `future_unit` VALUES ('d64646146195434ab7f52bf126746b43', 'QC_YS_001', '演示部门', '轻尘科技/演示部门', 'special', '1', 1001, NULL, '2020-07-13 00:00:00', '2020-07-13 10:13:25', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `future_unit` VALUES ('f3eb949451cb4c1ebb3d60c64f58b371', 'cw000002', '财务二部', '财务二部/财务二部/财务二部', 'main', '1e6d6fe4edbd41f788da5975dd89c4e4', 1, NULL, NULL, '2020-04-30 15:59:38', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);

-- ----------------------------
-- Table structure for future_user
-- ----------------------------
DROP TABLE IF EXISTS `future_user`;
CREATE TABLE `future_user`  (
  `userId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `userAccount` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户账户',
  `userName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `userPhone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话号码',
  `userPassword` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `userEmail` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱地址',
  `userBrithday` date NULL DEFAULT NULL COMMENT '生日',
  `userSex` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '性别',
  `userState` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登陆状态',
  `userAvatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像地址',
  `introduction` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简介',
  `motto` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '格言',
  `areaId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域id',
  `createTime` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人id',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `updateUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人id',
  `regTime` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  `cancellationTime` datetime(0) NULL DEFAULT NULL COMMENT '注销时间',
  `delFlag` tinyint(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '后台用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_user
-- ----------------------------
INSERT INTO `future_user` VALUES ('21dbcbb7889840359426b368c74a9375', 'zly', '久伴轻尘呀', '17764149196', 'a5926c02206dbaac6ee177e417dad9a6', '577955659@qq.com', '2020-06-05', 'MAN', '0', '75f988b150ed4aba83be84022cb517df', '呀呀呀', NULL, NULL, '2020-06-05 20:20:06', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-11 11:45:30', '21dbcbb7889840359426b368c74a9375', NULL, NULL, 0);
INSERT INTO `future_user` VALUES ('3752e7955b6444e9b9327b942c54861b', 'txl', '老谭', '13387659876', 'a5926c02206dbaac6ee177e417dad9a6', 'ddd@qq.com', '2020-05-20', 'MAN', '0', '06a9b90d198749abb3f475e370ae8027', '111111111', NULL, NULL, '2020-05-20 17:17:58', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-06-03 10:57:06', '3752e7955b6444e9b9327b942c54861b', NULL, NULL, 0);
INSERT INTO `future_user` VALUES ('a8121140323b4200aee4630e9f191379', 'demo', '演示账户', '17764149196', 'a5926c02206dbaac6ee177e417dad9a6', '577955659@qq.com', '2020-07-13', 'MAN', '0', '5873c8458f0d466a9426eb465cd12713', '呀呀呀呀', NULL, NULL, '2020-07-13 10:11:32', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-13 10:14:14', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL, 0);
INSERT INTO `future_user` VALUES ('ye6byKDjk14orM6IwaCUbCZCBV7205s2', 'qc', '久伴轻尘', '17764149196', 'a5926c02206dbaac6ee177e417dad9a6', 'jiubanqingchen@outlook.com', '1998-12-17', 'MAN', '1', 'ad29787aea614aecb5359fd7a5337298', '羽扇纶巾万书谋，厮杀谈笑几轻柔', NULL, '101', '2018-07-28 13:40:13', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-06-30 15:12:07', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL, 0);

-- ----------------------------
-- Table structure for future_user_dept
-- ----------------------------
DROP TABLE IF EXISTS `future_user_dept`;
CREATE TABLE `future_user_dept`  (
  `userDeptId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户部门id',
  `userId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `deptId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门id',
  `main` tinyint(1) NULL DEFAULT NULL COMMENT '是否主要部门',
  `valid` tinyint(1) NULL DEFAULT NULL COMMENT '是否有效',
  `timeEnd` date NULL DEFAULT NULL COMMENT '失效时间',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `updateUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`userDeptId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_user_dept
-- ----------------------------
INSERT INTO `future_user_dept` VALUES ('17ed3210a4794b60aca148571bcfc30d', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '1', 1, 1, '2020-06-25', '2020-01-08 17:44:29', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-06-04 15:43:07', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2');
INSERT INTO `future_user_dept` VALUES ('253e9ee9a4264f599fd544edc5f8d838', '3752e7955b6444e9b9327b942c54861b', 'd64646146195434ab7f52bf126746b43', 1, 1, NULL, '2020-07-13 11:37:45', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-13 11:37:53', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2');
INSERT INTO `future_user_dept` VALUES ('490b372c515e4d5f9daf229efe1cc7fc', 'a8121140323b4200aee4630e9f191379', 'd64646146195434ab7f52bf126746b43', 1, 1, NULL, '2020-07-13 10:13:34', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-13 11:38:02', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2');
INSERT INTO `future_user_dept` VALUES ('d35bacaa174f47ca85e6e5a714ef7664', '21dbcbb7889840359426b368c74a9375', '12', NULL, NULL, NULL, '2020-07-03 14:51:28', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', NULL, NULL);
INSERT INTO `future_user_dept` VALUES ('f15b8b69ea0e411382d65c8dcf4940a9', '21dbcbb7889840359426b368c74a9375', '1', 1, 1, '2026-06-05', '2020-06-05 20:20:06', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-06-05 20:20:19', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2');

-- ----------------------------
-- Table structure for future_user_group_role
-- ----------------------------
DROP TABLE IF EXISTS `future_user_group_role`;
CREATE TABLE `future_user_group_role`  (
  `userGroupRoleId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色用户关联id',
  `roleId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id',
  `userGroupId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id或者部门id',
  `userGroupType` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '关联类型',
  `createUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updateUser` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`userGroupRoleId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of future_user_group_role
-- ----------------------------
INSERT INTO `future_user_group_role` VALUES ('015ce989aa844e119c80626418182cc3', 'jiubanqingchen:admin', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', 'USER', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-01-03 17:37:26', NULL, NULL);
INSERT INTO `future_user_group_role` VALUES ('071c38215d284cff82912b00801c819c', 'jiubanqingchen:admin', '21dbcbb7889840359426b368c74a9375', 'USER', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-06-05 20:20:42', NULL, NULL);
INSERT INTO `future_user_group_role` VALUES ('230899bb289b41f0bdd896de92920643', 'e55c4dae7ab941619052c5a6d9067b1e', 'a8121140323b4200aee4630e9f191379', 'USER', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-13 10:14:06', NULL, NULL);
INSERT INTO `future_user_group_role` VALUES ('c3012bd0ab4245eeb1d3fbc541763219', 'e55c4dae7ab941619052c5a6d9067b1e', '3752e7955b6444e9b9327b942c54861b', 'USER', 'ye6byKDjk14orM6IwaCUbCZCBV7205s2', '2020-07-13 11:37:34', NULL, NULL);

-- ----------------------------
-- Function structure for getDictionaryDesc
-- ----------------------------
DROP FUNCTION IF EXISTS `getDictionaryDesc`;
delimiter ;;
CREATE FUNCTION `getDictionaryDesc`(categoryId varchar(50),id varchar(50))
 RETURNS varchar(200) CHARSET utf8
BEGIN
	DECLARE dictionaryName VARCHAR(200);
	SELECT DISTINCT codeName into dictionaryName from future_dictionary where codeItemId=categoryId and codeId=id;
	RETURN dictionaryName;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for getUserName
-- ----------------------------
DROP FUNCTION IF EXISTS `getUserName`;
delimiter ;;
CREATE FUNCTION `getUserName`(`id` varchar(32))
 RETURNS varchar(200) CHARSET utf8
BEGIN
	DECLARE user_name VARCHAR(200);
	SELECT userName into user_name from future_user where userId=id;
	RETURN user_name;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for test
-- ----------------------------
DROP FUNCTION IF EXISTS `test`;
delimiter ;;
CREATE FUNCTION `test`(rootId varchar(1000))
 RETURNS varchar(5000) CHARSET utf8
BEGIN
	#声明两个临时变量
	DECLARE temp VARCHAR(5000);
	DECLARE tempChd VARCHAR(5000);
	SET temp = '$';
	SET tempChd=CAST(rootId AS CHAR);#把rootId强制转换为字符
	WHILE tempChd is not null DO
	SET temp = CONCAT(temp,',',tempChd);#循环把所有节点连接成字符串。
	SELECT GROUP_CONCAT(menuId) INTO tempChd FROM future_menu where FIND_IN_SET(parentId,tempChd)>0;
	END WHILE;
	RETURN substring(temp, 3);
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
