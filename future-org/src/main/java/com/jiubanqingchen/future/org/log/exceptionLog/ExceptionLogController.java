package com.jiubanqingchen.future.org.log.exceptionLog;

import com.jiubanqingchen.future.model.models.ExceptionLog;
import com.jiubanqingchen.future.org.admin.ModelController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;

@AuthorityKey("org:operateLog")
public class ExceptionLogController extends ModelController<ExceptionLogService, ExceptionLog> {

}