package com.jiubanqingchen.future.org.authority.menu;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.future.common.tool.TreeJson;
import com.jiubanqingchen.future.model.models.Menu;
import com.jiubanqingchen.future.model.models.User;
import com.jiubanqingchen.future.org.admin.ModelService;
import com.jiubanqingchen.future.org.annotation.Sqlkey;

import java.util.List;

@Sqlkey("menu.list")
public class MenuService extends ModelService<Menu> {

    /**
     * 获取树形菜单
     *
     * @param kv 页面数据
     * @return 返回值
     */
    public List<Record> menuTree(Kv kv) {
        return new TreeJson("value").getTreeData(Db.find(Db.getSqlPara("menu.menuTree", kv)), "jxhx");
    }

    @Override
    public void afterDelete(Menu menu, Record record, User _usr) throws Exception {
        //菜单删除完成之后进行权限资源关联删除
        delete("future_resource_authority", "resourceId", menu.getMenuId(), _usr);
        super.afterDelete(menu, record, _usr);
    }
}
