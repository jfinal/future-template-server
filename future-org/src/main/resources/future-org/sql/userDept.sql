###基础查询
#sql("list")
	SELECT 
		`future_user_dept`.*,
	    (select userName from future_user where `future_user_dept`.userId=`future_user`.userId) as userName,
        (select unitName from future_unit where `future_user_dept`.deptId=`future_unit`.unitId) as unitName
	FROM 
		future_user_dept AS `future_user_dept`
	WHERE 1=1
		#@userDeptWheres()
		#@userDeptOrderby()
#end

#define userDeptWheres()
	#if(id)
		AND `future_user_dept`.userDeptId=#para(id)
	#end
    #if(userId)
        AND `future_user_dept`.userId=#para(userId)
    #end
#end

#define userDeptOrderby()

#end