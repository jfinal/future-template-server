#sql("list")###用户查询sql
SELECT 
	`future_user`.*,
	 getDictionaryDesc('ORG_USER_SEX',`future_user`.userSex) as userSexDesc
FROM 
	future_user AS `future_user`
WHERE 1=1
	#@userWheres()
	#@userOrderby()
#end

#define userWheres()
	#if(id)
		AND `future_user`.userId=#para(id)
	#end
	
	#if(userAccount)
		AND `future_user`.userAccount  #paraLike(userAccount)
	#end
	
	#if(userName)
		AND `future_user`.userName #paraLike(userName)
	#end
	
	#if(userSex)
		AND `future_user`.userSex=#para(userSex)
	#end

	#if(userPhone)
		AND `future_user`.userPhone #paraLike(userPhone)
	#end

	#if(userEmail)
		AND `future_user`.userEmail #paraLike(userEmail)
	#end

	#if(userBrithday)
		AND `future_user`.userBrithday > #para(userBrithday)
	#end

	#if(unitId && unitId!="1")###链表查询当前部门下的用户信息(如果为轻尘科技则查询所有数据)
        AND exists (select 1 from future_user_dept fud where fud.deptId=#para(unitId) and fud.userId=`future_user`.userId)
	#end

    #if(excludeUnitId)###排除已经与当前部门关联的用户
        AND not exists (select 1 from future_user_dept fud where fud.deptId=#para(excludeUnitId) and fud.userId=`future_user`.userId)
    #end

    #if(excludeRoleId)###排除已经与当前角色关联的用户
        AND not exists (select 1 from future_user_group_role fugr where fugr.roleId=#para(excludeRoleId) and fugr.userGroupId=`future_user`.userId)
    #end
#end

#define userOrderby()
    #if(prop)
	    order by tableName.#(prop) #if(sort) #(sort) #end
	#end
#end

#sql("login")
SELECT 
	`future_user`.*
FROM 
	future_user AS `future_user`
WHERE userAccount=? and userPassword=?
#end

###得到用户角色信息(包含其部门角色)
#sql("getUserRole")
select fr.roleId,fr.roleName,fr.roleDescription from future_role fr
      inner join future_user_group_role fugr on fugr.roleId=fr.roleId
      where fugr.userGroupId=?
      OR EXISTS (SELECT 1 FROM future_user_dept fud WHERE	fud.userId =? AND fud.deptId = fugr.userGroupId)
#end

###导出用户信息
#sql("exportUser")
      SELECT userAccount,userName,userPhone,userEmail,userBrithday,
           getDictionaryDesc('ORG_USER_SEX',`future_user`.userSex) as userSexDesc
      FROM future_user AS `future_user`
WHERE 1=1
	#@userWheres()
#end