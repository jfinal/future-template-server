package com.jiubanqingchen.future.org.authority.resourceAuthority;

import com.jiubanqingchen.future.model.models.ResourceAuthority;
import com.jiubanqingchen.future.org.admin.ModelController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;

@AuthorityKey("org:resourceAuthority")
public class ResourceAuthorityController extends ModelController<ResourceAuthorityService, ResourceAuthority> {

}