###查询当库所有表
#sql("listTable")
	SELECT
	       TABLE_NAME AS tableName,TABLE_COMMENT AS comment,engine as engine,create_time as createTime,update_time as updateTime,table_collation as collation
	FROM information_schema.TABLES as t where t.table_schema=#para(tableSchema)

    #if(likeAll)
        and (t.TABLE_NAME #paraLike(likeAll) or t.TABLE_COMMENT #paraLike(likeAll) )
    #end
#end

#define listTableWheres()

#end

#define listTableOrder()
     #if(prop)
        order by t.#(prop) #if(sort) #(sort) #end
     #end
#end

###查询当前表信息
#sql("getTableByName")
	SELECT TABLE_NAME AS tableName,TABLE_COMMENT AS comment FROM information_schema.TABLES where table_schema=?
	and table_name=?
#end

###得到表的主键
#sql("getPrimaryColumn")
	SELECT
		column_name as primaryName
	FROM
		information_schema.KEY_COLUMN_USAGE
	WHERE
		constraint_name = 'PRIMARY'
	    AND table_schema = ? 
		AND table_name = ? 
#end

#sql("listColumn")
	SELECT
		column_name AS name,
		column_comment AS comment,
		data_type as type,
		is_nullable AS isNull,
		column_type AS columnType,
		column_key as columnKey
	FROM
		information_schema.COLUMNS 
	WHERE
		 table_schema = ? 
		 AND table_name = ?
	ORDER BY ORDINAL_POSITION
#end