package com.jiubanqingchen.future.cms.business.common;

import com.jfinal.aop.Aop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.future.common.enums.CacheName;
import com.jiubanqingchen.future.model.models.Attachment;
import com.jiubanqingchen.future.org.admin.BaseService;
import com.jiubanqingchen.future.org.business.attachment.AttachmentService;
import net.coobird.thumbnailator.Thumbnails;

import java.io.File;
import java.util.List;

/**
 * @author light_dust
 * @ClassName: CommonService
 * @Description: (admin 公共数据管理)
 * @date 2019年10月26日 下午10:46:12
 */
public class CommonService extends BaseService {

    /**
     * @param @param  codeItemId
     * @param @return 参数
     * @return List<Record>    返回类型
     * @throws
     * @Title: listDictionary
     * @Description: (查询数据字典信息)
     * @author light_dust
     * @date 2019年10月26日 下午10:45:57
     */
    public List<Record> listDictionary(String codeItemId) {
        return Db.findByCache(CacheName.DICTIONARY_LIST.getValue(), codeItemId, Db.getSql("common.listDictionary"), codeItemId);
    }

    /**
     * 获取图片缩略图
     *
     * @param fileId 文件id
     * @param width  宽
     * @param height 高
     * @return
     * @throws Exception
     */
    public File getThumbImg(String fileId, int width, int height) throws Exception {
        AttachmentService attachmentService = Aop.get(AttachmentService.class);
        Attachment attachment = Attachment.dao.findById(fileId);
        if (attachment == null) return null;//无文件则返回
        String mime = attachment.getMimeType();
        if (mime != null && mime.startsWith("image")) {//判断是否属于图片类型
            File ofile = new File(PropKit.get("uploadUrl") + attachment.getFilePath());
            if (!ofile.exists()) return null;
            String prefix = "thub_w" + width + "h" + height;
            File thubfile = new File(ofile.getParent() + System.getProperty("file.separator") + prefix + ofile.getName());
            if (!thubfile.exists()) {
                Thumbnails.of(ofile).size(width, height).toFile(thubfile);
            }
            return thubfile;
        }
        return null;
    }


}
