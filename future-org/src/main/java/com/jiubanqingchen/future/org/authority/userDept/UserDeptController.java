package com.jiubanqingchen.future.org.authority.userDept;

import com.jiubanqingchen.future.model.models.UserDept;
import com.jiubanqingchen.future.org.admin.ModelController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;

@AuthorityKey("org:userDept")
public class UserDeptController extends ModelController<UserDeptService, UserDept> {

}