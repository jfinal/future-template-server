package com.jiubanqingchen.future.cms.config;

import com.jfinal.config.Routes;
import com.jiubanqingchen.future.cms.business.common.CommonController;
import com.jiubanqingchen.future.cms.business.cronTask.CronTaskController;
import com.jiubanqingchen.future.cms.business.dictionary.DictionaryController;
import com.jiubanqingchen.future.cms.business.dictionarytype.DictionaryTypeController;

public class CmsRoute extends Routes {
    @Override
    public void config() {
        this.add("/cms/dictionary", DictionaryController.class);
        this.add("/cms/dictionaryType", DictionaryTypeController.class);
        this.add("/cms/common", CommonController.class);
        this.add("/cms/cronTask", CronTaskController.class);
    }
}
