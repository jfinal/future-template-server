###查询数据字典信息
#sql("listDictionary")
            SELECT
    `future_dictionary`.codeName as label,`future_dictionary`.codeId as value
FROM
    future_dictionary AS `future_dictionary`
WHERE
    `future_dictionary`.codeItemId =?
ORDER BY
    `future_dictionary`.sortNo ASC
#end

###通用条件删除
#sql("delete_where")
DELETE FROM #(tableName) where #(field) = #para(fieldValue)
#end

###in查询
#define genIn(ids)
            in (
	#for (id : ids)
		#(for.first ? "" : ",") #para(id)
	#end
   )
#end

