###基础查询
#sql("list")
	SELECT 
		`future_unit`.*
		,getDictionaryDesc('UNIT_TYPE',`future_unit`.unitType) as unitTypeDesc
	    ,(select unitName from future_unit fu where fu.unitId=`future_unit`.parentId) as parentUnitName
        ,(select count(*) from future_unit fu where fu.parentId=`future_unit`.unitId) as leaf
	FROM 
		future_unit AS `future_unit`
	WHERE 1=1
		#@unitWheres()
		#@unitOrderby()
#end

#define unitWheres()
	#if(id)
		AND `future_unit`.unitId=#para(id)
	#end

	#if(unitName)
		AND `future_unit`.unitName #paraLike(unitName)
	#end
	#if(unitType)
		AND `future_unit`.unitType=#para(unitType)
	#end
    #if(parentId)
        AND `future_unit`.parentId=#para(parentId)
    #end
#end

#define unitOrderby()
	#if(prop)
	    order by `future_unit`.#(prop) #if(sort) #(sort) #end
	#else
        order by `future_unit`.sortNo
    #end
#end