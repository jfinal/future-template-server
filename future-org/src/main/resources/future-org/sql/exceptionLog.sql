###基础查询
#sql("list")
	SELECT 
		`future_exception_log`.*
	FROM 
		future_exception_log AS `future_exception_log`
	WHERE 1=1
		#@exceptionLogWheres()
		#@exceptionLogOrderby()
#end

#define exceptionLogWheres()
	#if(id)AND `future_exception_log`.exceptionLogId=#para(id)#end

	#if(loginUserName)
	    AND `future_exception_log`.loginUserName #paraLike(loginUserName)
	#end
	#if(loginIp)
		AND `future_exception_log`.loginIp #paraLike(loginIp)
	#end
	#if(loginArea)
		AND `future_exception_log`.loginArea #paraLike(loginArea)
	#end
	#if(requestUrl)
		AND `future_exception_log`.requestUrl #paraLike(requestUrl)
	#end
#end

#define exceptionLogOrderby()
	#if(prop)
		order by `future_exception_log`.#(prop) #(sort)
	#else
	    order by `future_exception_log`.createTime desc
	#end
#end