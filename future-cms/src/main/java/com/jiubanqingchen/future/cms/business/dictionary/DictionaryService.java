package com.jiubanqingchen.future.cms.business.dictionary;


import com.jiubanqingchen.future.model.models.Dictionary;
import com.jiubanqingchen.future.org.admin.ModelService;
import com.jiubanqingchen.future.org.annotation.Sqlkey;

/**
 * @ClassName: DictionaryService  
 * @Description: (数据字典)  
 * @author Light_Dust  
 * @date 2019年7月4日 下午1:40:55
 */
@Sqlkey("dictionary.list")
public class DictionaryService extends ModelService<Dictionary> {

}
