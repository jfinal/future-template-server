package com.jiubanqingchen.future.generator.config;

import com.jfinal.config.Routes;
import com.jiubanqingchen.future.generator.create.CreateController;
import com.jiubanqingchen.future.generator.table.TableController;

public class GeneratorRoute extends Routes {
    @Override
    public void config() {
        this.add("/generator/table", TableController.class);
        this.add("/generator/create", CreateController.class);
    }
}
