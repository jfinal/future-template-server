package com.jiubanqingchen.future.org.admin;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.future.model.models.User;

import java.util.List;

public interface IBaseService {

    void delete(String tableName, String field, Object fieldValue, User _usr);

    <T> List<T> exportData(Kv kv) throws Exception;

    <T> List<T> exportData(String sqlKey, Kv kv, Class<T> clazz) throws Exception;

    List<Record> list(Kv kv) throws Exception;

    List<Record> list(String sqlKey, Kv kv) throws Exception;

    List<Record> list(String sqlKey, Object... params) throws Exception;

    Record id(String id, Kv kv) throws Exception;

    Record id(String sqlKey, String id, Kv kv) throws Exception;

    Page<Record> page(int pageNum, int limit, Kv kv) throws Exception;

    Page<Record> page(String sqlKey, int pageNum, int limit, Kv kv) throws Exception;
}
