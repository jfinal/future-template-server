package com.jiubanqingchen.future.cms.business.dictionary;

import com.jiubanqingchen.future.model.models.Dictionary;
import com.jiubanqingchen.future.org.admin.ModelController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;

/**
 * @ClassName: DictionaryController  
 * @Description: (字典类)  
 * @author Light_Dust  
 * @date 2019年7月4日 下午1:35:39
 */
@AuthorityKey("cms:dictionary")
public class DictionaryController extends ModelController<DictionaryService, Dictionary> {

}
