###数据字典类型
#namespace("menu")
#include("sql/menu.sql")
#end

### 公共数据管理
#namespace("common")
#include("sql/common.sql")
#end

###用户
#namespace("user")
#include("sql/user.sql")
#end

###附件管理
#namespace("attachment")
#include("sql/attachment.sql")
#end

###部门管理
#namespace("unit")
#include("sql/unit.sql")
#end

###模块管理
#namespace("module")
#include("sql/module.sql")
#end

###用户部门管理
#namespace("userDept")
#include("sql/userDept.sql")
#end

###角色管理
#namespace("role")
#include("sql/role.sql")
#end

###用户/部门角色管理
#namespace("userGroupRole")
#include("sql/userGroupRole.sql")
#end

###角色资源管理
#namespace("resourceAuthority")
#include("sql/resourceAuthority.sql")
#end

###登录日志管理
#namespace("loginLog")
#include("sql/loginLog.sql")
#end

###操作日志管理
#namespace("operateLog")
#include("sql/operateLog.sql")
#end

###异常日志管理
#namespace("exceptionLog")
#include("sql/exceptionLog.sql")
#end

###匿名访问
#namespace("anonymous")
#include("sql/anonymous.sql")
#end


