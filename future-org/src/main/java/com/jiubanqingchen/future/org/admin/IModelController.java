package com.jiubanqingchen.future.org.admin;


public interface IModelController<T> {

    void add() throws Exception;

    void addBatch() throws Exception;

    void delete() throws Exception;

    void deletes() throws Exception;

    void list() throws Exception;

    void id() throws Exception;

    void page() throws Exception;

    void update() throws Exception;

    void updateBatch() throws Exception;

}
