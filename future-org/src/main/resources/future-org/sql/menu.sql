###附件表基础查询
#sql("list")
SELECT
    `future_menu`.*,
    (select title from future_menu fm where fm.menuId=`future_menu`.parentId) as parentTitle
FROM
    future_menu AS `future_menu`
WHERE 1=1
  #@menuWheres()
  #@menuOrderBy()
#end

#sql("menuTree")
SELECT
    `future_menu`.menuId as value,
    `future_menu`.title as label,
    `future_menu`.*
FROM
    future_menu AS `future_menu`
  #@menuOrderBy()
#end

#define menuWheres()
  #if(id)
    AND `future_menu`.menuId =#para(id)
  #end
  #if(title)
    AND `future_menu`.title #paraLike(title)
  #end
  #if(menuType)
    AND `future_menu`.menuType =#para(menuType)
  #end
#end

#define menuOrderBy()
    order by `future_menu`.sortNo
#end