package com.jiubanqingchen.future.org.business.anonymous;

import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.future.common.enums.WebSocketCategory;
import com.jiubanqingchen.future.common.tool.WebSocketTool;
import com.jiubanqingchen.future.model.models.Anonymous;
import com.jiubanqingchen.future.org.admin.ModelController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;

@AuthorityKey("org:anonymous")
public class AnonymousController extends ModelController<AnonymousService, Anonymous> {

    public void test() throws Exception {
        String message = "websocket信息已经发送成功";
        Record record = new Record();
        record.set("code", WebSocketCategory.MESSAGE.getCode()).set("msg", WebSocketCategory.MESSAGE.getMsg());
        WebSocketTool.sendMessageAll(record);
        renderResult(message);
    }

}