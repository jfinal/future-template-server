package com.jiubanqingchen.future.org.admin;

import com.jfinal.plugin.activerecord.Model;
import com.jiubanqingchen.future.common.enums.SystemStatus;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.ParameterizedType;
import java.util.List;

public class ModelController<T extends ModelService, M extends Model> extends BaseController implements IModelController {
    private static Logger logger = LoggerFactory.getLogger(ModelController.class);

    protected T service;

    private Class<M> modelClazz;

    public ModelController() {
        try {
            service = ((Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0]).newInstance();
        } catch (Exception e) {
            logger.info("service创建失败", e);
        }
    }

    protected Class<M> getModelClazz() {
        if (modelClazz == null) {
            modelClazz = (Class<M>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
        }
        return modelClazz;
    }

    @Override
    @AuthorityKey("add")
    public void add() throws Exception {
        renderResult(SystemStatus.INSERT_SUCCESS, service.insert(getModel(), getRecordExtData(), getCurrentUser()));
    }

    @Override
    @AuthorityKey("add")
    public void addBatch() throws Exception {
        renderResult(SystemStatus.INSERT_SUCCESS, service.insertBatch(getModelArray(), getRecordExtData(), getCurrentUser()));
    }

    @Override
    @AuthorityKey("delete")
    public void delete() throws Exception {
        renderResult(SystemStatus.DELETE_SUCCESS, service.delete(getModel(), getRecordExtData(), getCurrentUser()));
    }

    @Override
    @AuthorityKey("delete")
    public void deletes() throws Exception {
        renderResult(SystemStatus.DELETE_SUCCESS, service.deleteBatch(getModelArray(), getRecordExtData(), getCurrentUser()));
    }

    @Override
    @AuthorityKey("view")
    public void list() throws Exception {
        renderResult(service.list(getKv()));
    }

    @Override
    @AuthorityKey("view")
    public void id() throws Exception {
        renderResult(service.id(getPara(), getKv()));
    }

    @Override
    @AuthorityKey("view")
    public void page() throws Exception {
        renderResult(service.page(getInt("page", 1), getInt("limit", 10), getKv()));
    }

    @Override
    @AuthorityKey("update")
    public void update() throws Exception {
        renderResult(SystemStatus.UPDATE_SUCCESS, service.update(getModel(), getRecordExtData(), getCurrentUser()));
    }

    @Override
    public void updateBatch() throws Exception {
        renderResult(SystemStatus.UPDATE_SUCCESS, service.updateBatch(getModelArray(), getRecordExtData(), getCurrentUser()));
    }

    protected List<M> getModelArray() {
        return getRawDataForArray(getModelClazz());
    }

    protected M getModel() {
        return (M) getRawDataForModel(getModelClazz());
    }
}
