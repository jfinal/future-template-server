package com.jiubanqingchen.future.common.enums;

/**
 * @ClassName: RedisPrefix  
 * @Description: (redis缓存前缀)  
 * @author Light_Dust  
 * @date 2019年7月4日 下午1:52:58
 */
public enum RedisPrefix {
	/**token储存前缀*/
	TOKEN("token:"),
	/**用户信息前缀*/
	USER_INFO("user:info:");
	
	RedisPrefix(String value){
		this.value=value;
	}
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	

}
