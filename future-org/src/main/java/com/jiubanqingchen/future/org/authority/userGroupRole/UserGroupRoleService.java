package com.jiubanqingchen.future.org.authority.userGroupRole;

import com.jiubanqingchen.future.model.models.UserGroupRole;
import com.jiubanqingchen.future.org.admin.ModelService;
import com.jiubanqingchen.future.org.annotation.Sqlkey;

@Sqlkey("userGroupRole.list")
public class UserGroupRoleService extends ModelService<UserGroupRole> {
	
	
}