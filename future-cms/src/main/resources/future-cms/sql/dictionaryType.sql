#sql("list")
SELECT
	`future_dictionary_type`.*
FROM
    future_dictionary_type AS `future_dictionary_type`
WHERE 1=1
	#@dictionaryTypeWheres()
	#@dictionaryTypeOrderby()
#end

###条件
#define dictionaryTypeWheres()
	#if(id)
		AND `future_dictionary_type`.dictionaryTypeId=#para(id)
	#end
	#if(codeItemId)
		AND `future_dictionary_type`.codeItemId =#para(codeItemId)
	#end
	#if(codeItemIdLike)
		AND `future_dictionary_type`.codeItemId #paraLike(codeItemIdLike)
	#end
	#if(codeItemName)
		AND `future_dictionary_type`.codeItemName #paraLike(codeItemName)
	#end
	#if(isUpdate)
		AND `future_dictionary_type`.isUpdate =#(isUpdate)
	#end
	#if(parentId)
		AND `future_dictionary_type`.parentId =#para(parentId)
	#end
	#if(likeAll)
		AND (`future_dictionary_type`.codeItemName #paraLike(likeAll) or `future_dictionary_type`.codeItemId #paraLike(likeAll)
    #end
#end

#define dictionaryTypeOrderby()
    #if(prop)
        order by `future_dictionary_type`.#(prop) #if(sort) #(sort) #end
    #else
        order by `future_dictionary_type`.sortNo
    #end
#end

