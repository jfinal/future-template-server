package com.jiubanqingchen.future.model.models.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseDictionary<M extends BaseDictionary<M>> extends Model<M> implements IBean {

	public M setDictionaryId(java.lang.String dictionaryId) {
		set("dictionaryId", dictionaryId);
		return (M)this;
	}
	
	public java.lang.String getDictionaryId() {
		return getStr("dictionaryId");
	}

	public M setCodeItemId(java.lang.String codeItemId) {
		set("codeItemId", codeItemId);
		return (M)this;
	}
	
	public java.lang.String getCodeItemId() {
		return getStr("codeItemId");
	}

	public M setCodeId(java.lang.String codeId) {
		set("codeId", codeId);
		return (M)this;
	}
	
	public java.lang.String getCodeId() {
		return getStr("codeId");
	}

	public M setCodeName(java.lang.String codeName) {
		set("codeName", codeName);
		return (M)this;
	}
	
	public java.lang.String getCodeName() {
		return getStr("codeName");
	}

	public M setSortNo(java.lang.Integer sortNo) {
		set("sortNo", sortNo);
		return (M)this;
	}
	
	public java.lang.Integer getSortNo() {
		return getInt("sortNo");
	}

	public M setIsUpdate(java.lang.String isUpdate) {
		set("isUpdate", isUpdate);
		return (M)this;
	}
	
	public java.lang.String getIsUpdate() {
		return getStr("isUpdate");
	}

	public M setCreateTime(java.util.Date createTime) {
		set("createTime", createTime);
		return (M)this;
	}
	
	public java.util.Date getCreateTime() {
		return get("createTime");
	}

	public M setCreateUser(java.lang.String createUser) {
		set("createUser", createUser);
		return (M)this;
	}
	
	public java.lang.String getCreateUser() {
		return getStr("createUser");
	}

	public M setUpdateTime(java.util.Date updateTime) {
		set("updateTime", updateTime);
		return (M)this;
	}
	
	public java.util.Date getUpdateTime() {
		return get("updateTime");
	}

	public M setUpdateUser(java.lang.String updateUser) {
		set("updateUser", updateUser);
		return (M)this;
	}
	
	public java.lang.String getUpdateUser() {
		return getStr("updateUser");
	}

}
