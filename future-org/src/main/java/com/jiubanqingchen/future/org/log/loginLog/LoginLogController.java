package com.jiubanqingchen.future.org.log.loginLog;

import com.jiubanqingchen.future.model.models.LoginLog;
import com.jiubanqingchen.future.org.admin.ModelController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;

@AuthorityKey("org:loginLog")
public class LoginLogController extends ModelController<LoginLogService, LoginLog> {

}