package com.jiubanqingchen.future.org.annotation;

import java.lang.annotation.*;

@Inherited
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthorityKey {
    String value();
}
