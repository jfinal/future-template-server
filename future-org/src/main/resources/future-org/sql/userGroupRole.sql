###基础查询
#sql("list")
    select `future_user_group_role`.*,
        CASE
		WHEN userGroupType = 'DEPT' THEN
		`future_unit`.unitName
		WHEN userGroupType = 'USER' THEN
		`future_user`.userName
	END AS userGroupName
        ,getDictionaryDesc('ORG_USER_ROLE_GROUP_TYPE',`future_user_group_role`.userGroupType) as userGroupTypeDesc,
        `future_role`.roleName, `future_role`.roleDescription, `future_role`.isUpdate
    from future_user_group_role `future_user_group_role`
    LEFT JOIN future_unit `future_unit` ON `future_user_group_role`.userGroupId = `future_unit`.unitId
    LEFT JOIN future_user `future_user` ON `future_user_group_role`.userGroupId = `future_user`.userId
    LEFT JOIN future_role `future_role` ON `future_user_group_role`.roleId=`future_role`.roleId
    WHERE 1=1
     #@userGroupRoleWheres()
     #@userGroupRoleOrderby()
#end

#define userGroupRoleWheres()
	#if(id)
		AND `future_user_group_role`.userGroupRoleId=#para(id)
	#end

	#if(userGroupType)
		AND `future_user_group_role`.userGroupType=#para(userGroupType)
	#end

	#if(roleId)
        AND `future_user_group_role`.roleId=#para(roleId)
	#end
	#if(userId)
        AND `future_user_group_role`.userGroupId=#para(userId)
	#end
#end

#define userGroupRoleOrderby()
	#if(prop)
		order by `future_user_group_role`.#(prop) #(sort)
	#end
#end