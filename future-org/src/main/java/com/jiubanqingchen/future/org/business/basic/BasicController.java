package com.jiubanqingchen.future.org.business.basic;

import com.jfinal.aop.Clear;
import com.jiubanqingchen.future.org.admin.BaseController;

/**
 * 全局通用处理类不需要任何权限即可放行
 */
@Clear
public class BasicController extends BaseController {

    /**
     * 生成二维码
     */
    public void renderCaptcha() {
        super.renderCaptcha();
    }

}
