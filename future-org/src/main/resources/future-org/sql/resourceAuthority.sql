###基础查询
#sql("list")
	SELECT 
		`future_resource_authority`.*
	FROM 
		future_resource_authority AS `future_resource_authority`
	WHERE 1=1
		#@resourceAuthorityWheres()
		#@resourceAuthorityOrderby()
#end

###获取角色权限资源id
#sql("getResourceIdsByRoleId")
SELECT
    `future_resource_authority`.resourceId
FROM
    future_resource_authority AS `future_resource_authority`
WHERE 1=1
    #@resourceAuthorityWheres()
#end

#define resourceAuthorityWheres()
	#if(id)
		AND `future_resource_authority`.resourceAuthorityId=#para(id)
	#end
    #if(roleId)
        AND `future_resource_authority`.roleId=#para(roleId)
    #end
#end

#define resourceAuthorityOrderby()
	#if(prop)
		order by `future_resource_authority`.#(prop) #(sort)
	#end
#end