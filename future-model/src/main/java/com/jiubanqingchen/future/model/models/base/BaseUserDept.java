package com.jiubanqingchen.future.model.models.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseUserDept<M extends BaseUserDept<M>> extends Model<M> implements IBean {

	public M setUserDeptId(java.lang.String userDeptId) {
		set("userDeptId", userDeptId);
		return (M)this;
	}
	
	public java.lang.String getUserDeptId() {
		return getStr("userDeptId");
	}

	public M setUserId(java.lang.String userId) {
		set("userId", userId);
		return (M)this;
	}
	
	public java.lang.String getUserId() {
		return getStr("userId");
	}

	public M setDeptId(java.lang.String deptId) {
		set("deptId", deptId);
		return (M)this;
	}
	
	public java.lang.String getDeptId() {
		return getStr("deptId");
	}

	public M setMain(java.lang.Boolean main) {
		set("main", main);
		return (M)this;
	}
	
	public java.lang.Boolean getMain() {
		return get("main");
	}

	public M setValid(java.lang.Boolean valid) {
		set("valid", valid);
		return (M)this;
	}
	
	public java.lang.Boolean getValid() {
		return get("valid");
	}

	public M setTimeEnd(java.util.Date timeEnd) {
		set("timeEnd", timeEnd);
		return (M)this;
	}
	
	public java.util.Date getTimeEnd() {
		return get("timeEnd");
	}

	public M setCreateTime(java.util.Date createTime) {
		set("createTime", createTime);
		return (M)this;
	}
	
	public java.util.Date getCreateTime() {
		return get("createTime");
	}

	public M setCreateUser(java.lang.String createUser) {
		set("createUser", createUser);
		return (M)this;
	}
	
	public java.lang.String getCreateUser() {
		return getStr("createUser");
	}

	public M setUpdateTime(java.util.Date updateTime) {
		set("updateTime", updateTime);
		return (M)this;
	}
	
	public java.util.Date getUpdateTime() {
		return get("updateTime");
	}

	public M setUpdateUser(java.lang.String updateUser) {
		set("updateUser", updateUser);
		return (M)this;
	}
	
	public java.lang.String getUpdateUser() {
		return getStr("updateUser");
	}

}
