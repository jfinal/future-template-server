package com.jiubanqingchen.future.common.tool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.*;
import java.text.DecimalFormat;

public class FileTool {
  private static Logger logger = LoggerFactory.getLogger(FileTool.class);
  /**
   * @Title: judgeDirExists
   * @Description: (判断文件夹是否存在,不存在进行创建)
   * @param
   * @return void 返回类型
   * @throws @author Light_Dust
   * @date 2019年5月11日 下午5:40:29
   */
  public static void judgeDirExists(File file) {
    if (file.exists()) {
      if (!file.isDirectory()) {
        logger.info("存在同名文件,文件夹不需要创建");
      }
    } else{
      logger.info("文件夹不存在, 创建中 ..." + file.getPath());
      file.setWritable(true, false);
      file.mkdirs();
      logger.info("文件夹创建成功");
    }
  }

  /**
   * @Title: isImage
   * @Description: (判断文件是否为图片)
   * @param @param imageFile
   * @param @return 参数
   * @return boolean 返回类型
   * @throws @author Light_Dust
   * @date 2019年6月25日 下午7:42:41
   */
  public static boolean isImage(File imageFile) {
    if (!imageFile.exists()) {
      return false;
    }
    Image img = null;
    try {
      img = ImageIO.read(imageFile);
      if (img == null || img.getWidth(null) <= 0 || img.getHeight(null) <= 0) {
        return false;
      }
      return true;
    } catch (Exception e) {
      logger.error("图片识别失败", e);
      return false;
    }
  }

  /**
   * 得到文件大小 B KB MB GB
   * @param size
   * @return
   */
  public static String getFileSize(long size){
    if(size <= 0) return "0";
    final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
    int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
    return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
  }


  /**
   * @Title: createFile
   * @Description: (生成文件)
   * @param @param dir 文件夹名称
   * @param @param fileName 文件名称
   * @param @return 参数
   * @return File 返回类型
   * @throws @author Light_dust
   * @date 2019年9月16日 下午4:07:33
   */
  public static File createFile(String dir, String fileName) {
    FileTool.judgeDirExists(new File(dir));
    logger.info("文件路径：" + dir);
    logger.info("文件名：" + fileName);
    return new File(dir + fileName);
  }

  /**
   * 
   * @Title: deleteFile
   * @Description: (删除文件)
   * @param @param file 文件
   * @param @return 参数
   * @return boolean 返回类型
   * @throws @author light_dust
   * @date 2019年10月16日 下午8:17:55
   */
  public static boolean deleteFile(File file) {
    if (!file.exists()) {
      return false;
    }
    if (file.isDirectory()) {
      File[] files = file.listFiles();
      for (File f : files) {
        deleteFile(f);
      }
    }
    return file.delete();
  }

  /**
   * @Title: deleteFile
   * @Description:(删除文件)
   * @param @param dir 路径
   * @param @return 参数
   * @return boolean 返回类型
   * @throws @author light_dust
   * @date 2019年10月16日 下午8:23:55
   */
  public static boolean deleteFile(String dir) {
    return deleteFile(new File(dir));
  }
}
