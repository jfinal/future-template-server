package com.jiubanqingchen.future.org.authority.user;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import com.jfinal.aop.Aop;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;

import com.jiubanqingchen.future.model.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class UserImportListener extends AnalysisEventListener<UserExportBean> {
    private static Logger logger = LoggerFactory.getLogger(UserImportListener.class);
    List<User> users = new ArrayList<>();

    @Override
    public void invoke(UserExportBean userExportBean, AnalysisContext analysisContext) {
        User user=JsonKit.parse(JSON.toJSONString(userExportBean),User.class);
        String userSexDesc=userExportBean.getUserSexDesc();
        if(StrKit.notBlank(userSexDesc)){
            if("男".equals(userSexDesc)){
                user.setUserSex("MAN");
            }else if("女".equals(userSexDesc)){
                user.setUserSex("WOMAN");
            }
        }
        users.add(user);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        logger.info("读取完毕");
        UserService userService = Aop.get(UserService.class);
        try {
            userService.insertBatch(users, new Record(), new User());
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        System.out.println(users.size());
    }
}
