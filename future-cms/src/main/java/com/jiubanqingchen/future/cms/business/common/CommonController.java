package com.jiubanqingchen.future.cms.business.common;

import com.jfinal.aop.Clear;
import com.jfinal.aop.Inject;
import com.jiubanqingchen.future.org.admin.BaseController;

import java.io.File;

/**
 * @author light_dust
 * @ClassName: CommonController
 * @Description: (公共处理类)
 * @date 2019年10月26日 下午10:39:22
 */
@Clear
public class CommonController extends BaseController {

    @Inject
    CommonService commonService;

    /**
     * @param
     * @return void    返回类型
     * @throws
     * @Title: listDictionary
     * @Description: (数据字典查询)
     * @author light_dust
     * @date 2019年10月27日 下午5:00:03
     */
    public void listDictionary() {
        renderResult(commonService.listDictionary(getPara()));
    }

    public void thumbImg() throws Exception {
        File file = commonService.getThumbImg(getPara("fileId"), getParaToInt("w"), getParaToInt("h"));
        if (file != null) {
            renderFile(file);
        } else {
            renderNull();
        }
    }
}
