package com.jiubanqingchen.future.org.admin;

import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.future.model.models.User;

import java.util.List;

/**
 * modelService
 *
 * @param <T>
 * @author light_dust
 * @Date 2020/5/21
 */
public interface IModelService<T> {

    void afterDelete(T model, Record record, User _usr) throws Exception;

    void afterInsert(T model, Record record, User _usr) throws Exception;

    void afterSave(T model, Record record, User _usr) throws Exception;

    void afterUpdate(T model, Record record, User _usr) throws Exception;

    boolean delete(T model, Record record, User _usr) throws Exception;

    boolean deleteBatch(List<T> modelList, Record record, User _usr) throws Exception;

    T insert(T model, Record record, User _usr) throws Exception;

    boolean insertBatch(List<T> modelList, Record record, User _usr) throws Exception;

    void preDelete(T model, Record record, User _usr) throws Exception;

    void preInsert(T model, Record record, User _usr) throws Exception;

    void preUpdate(T model, Record record, User _usr) throws Exception;

    T update(T model, Record record, User _usr) throws Exception;

    boolean updateBatch(List<T> modelList, Record record, User _usr) throws Exception;

}
