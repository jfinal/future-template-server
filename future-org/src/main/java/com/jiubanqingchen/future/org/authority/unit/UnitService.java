package com.jiubanqingchen.future.org.authority.unit;

import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.future.model.models.Unit;
import com.jiubanqingchen.future.model.models.User;
import com.jiubanqingchen.future.model.models.UserDept;
import com.jiubanqingchen.future.model.models.UserGroupRole;
import com.jiubanqingchen.future.org.admin.ModelService;
import com.jiubanqingchen.future.org.annotation.Sqlkey;

@Sqlkey("unit.list")
public class UnitService extends ModelService<Unit> {

    @Override
    public void afterDelete(Unit unit, Record record, User _usr) throws Exception {
        String unitId = unit.getUnitId();
        //删除部门后 将与部门相关连的数据删除
        delete(getTableName(UserDept.class), "deptId", unitId, _usr);
        //删除部门后 将与部门相关的角色数据删除
        delete(getTableName(UserGroupRole.class), "userGroupId", unitId, _usr);
        super.afterDelete(unit, record, _usr);
    }
}