###基础查询
#sql("list")
	SELECT 
		`future_operate_log`.*
	FROM 
		future_operate_log AS `future_operate_log`
	WHERE 1=1
		#@operateLogWheres()
		#@operateLogOrderby()
#end

#define operateLogWheres()
	#if(id)
		AND `future_operate_log`.operateLogId=#para(id)
	#end

	#if(loginUserName)
		AND `future_operate_log`.loginUserName #paraLike(loginUserName)
	#end
	#if(loginIp)
		AND `future_operate_log`.loginIp #paraLike(loginIp)
	#end
	#if(loginArea)
		AND `future_operate_log`.loginArea #paraLike(loginArea)
	#end
	#if(requestUrl)
		AND `future_operate_log`.requestUrl #paraLike(requestUrl)
	#end
#end

#define operateLogOrderby()
    #if(prop)
        order by `future_operate_log`.#(prop) #(sort)
    #else
        order by `future_operate_log`.createTime desc
    #end
#end