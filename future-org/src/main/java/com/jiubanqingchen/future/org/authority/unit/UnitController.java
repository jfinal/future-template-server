package com.jiubanqingchen.future.org.authority.unit;

import com.jiubanqingchen.future.model.models.Unit;
import com.jiubanqingchen.future.org.admin.ModelController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;

@AuthorityKey("org:unit")
public class UnitController extends ModelController<UnitService, Unit> {
}