package com.jiubanqingchen.future.cms.business.cronTask;

import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.future.common.plugin.quartz.QuartzManager;
import com.jiubanqingchen.future.model.models.CronTask;
import com.jiubanqingchen.future.model.models.User;
import com.jiubanqingchen.future.org.admin.ModelService;
import com.jiubanqingchen.future.org.annotation.Sqlkey;

@Sqlkey("cronTask.list")
public class CronTaskService extends ModelService<CronTask> {

    /**
     * 修改任务状态执行
     *
     * @param cronTaskId 定时任务id
     * @param active     是否激活
     */
    public boolean changeActive(String cronTaskId, boolean active) throws Exception {
        CronTask cronTask = CronTask.dao.findById(cronTaskId);
        if (cronTask == null) throw new Exception("当前任务" + cronTaskId + "不存在");
        if (cronTask.getActive() != active) {
            updateJobState(cronTask, active);
        }
        //开始执行状态更新
        cronTask.setActive(active).update();
        return true;
    }

    /**
     * 执行一次定时任务
     *
     * @param cronTaskId
     * @throws Exception
     */
    public boolean runOnceJob(String cronTaskId) throws Exception {
        CronTask cronTask = CronTask.dao.findById(cronTaskId);
        if (cronTask == null) throw new Exception("当前任务" + cronTaskId + "不存在");
        QuartzManager.runOnceJob(cronTask);
        return true;
    }

    /**
     * 更新任务状态
     *
     * @param cronTask
     * @param flag
     */
    private void updateJobState(CronTask cronTask, boolean flag) throws Exception{
        if (QuartzManager.getTriggerState(cronTask) == "NONE") {//当前任务不在任务队列
            if (flag == true) QuartzManager.addJob(cronTask);//新增新任务
        } else { //当前任务在任务队列
            if (flag) QuartzManager.resumeJob(cronTask);
            else QuartzManager.pauseJob(cronTask);
        }
    }

    @Override
    public void preUpdate(CronTask cronTask, Record record, User _usr) throws Exception {
        String cronTaskId = cronTask.getCronTaskId();//得到任务id
        //得到老的cronTask
        CronTask oldCronTask = CronTask.dao.findById(cronTaskId);
        if (oldCronTask == null) throw new Exception("数据异常 当前任务已经不存在,请检查数据");
        //比对新老数据 修改任务时间均修改任务
        if (!oldCronTask.getCronExpression().equals(cronTask.getCronExpression()))
            QuartzManager.updateCronExpression(cronTask);
        //修改任务状态
        boolean active = cronTask.getActive();
        if (oldCronTask.getActive() != active) updateJobState(cronTask, active);
        super.preUpdate(cronTask, record, _usr);
    }

    @Override
    public void afterInsert(CronTask cronTask, Record record, User _usr) throws Exception {
        if (cronTask.getActive() == true) {
            QuartzManager.addJob(cronTask);//添加定时任务
        }
        super.afterInsert(cronTask, record, _usr);
    }

    @Override
    public void preDelete(CronTask cronTask, Record record, User _usr) throws Exception {
        QuartzManager.removeJob(cronTask);
        super.preDelete(cronTask, record, _usr);
    }
}
