package com.jiubanqingchen.future.cms.business.cronTask;

import com.jiubanqingchen.future.model.models.CronTask;
import com.jiubanqingchen.future.org.admin.ModelController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;

@AuthorityKey("cms:cronTask")
public class CronTaskController extends ModelController<CronTaskService,CronTask> {

    @AuthorityKey("update")
    public void changeActive() throws Exception {
        renderResult(service.changeActive(get("cronTaskId"), getBoolean("active")));
    }

    @AuthorityKey("update")
    public void runOnceJob() throws Exception {
        renderResult(service.runOnceJob(get("cronTaskId")));
    }
}
