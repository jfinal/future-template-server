package com.jiubanqingchen.future.org.admin;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.future.common.enums.SystemStatus;
import com.jiubanqingchen.future.model.models.User;

import java.util.List;


public interface IBaseController {
	
	void renderResult(Object obj);
	
	void renderResult(SystemStatus systemStatus);
	
	void renderResult(SystemStatus systemStatus, Object obj);

	Record getRecordExtData();

	Record getRecordRawData();

	
	<T> T getRawDataForObject(Class<?> clazz);

	Model getRawDataForModel(Class<?> clazz);

	<T> List<?> getRawDataForArray(Class<?> clazz);

	<T> List<Model<?>> getRawDataForModelArray(Class<?> clazz);
	
	String getCurrentToken();
	
	User getCurrentUser();
}
