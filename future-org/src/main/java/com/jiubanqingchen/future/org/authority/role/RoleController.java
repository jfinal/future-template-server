package com.jiubanqingchen.future.org.authority.role;

import com.jiubanqingchen.future.model.models.Role;
import com.jiubanqingchen.future.org.admin.ModelController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;

@AuthorityKey("org:role")
public class RoleController extends ModelController<RoleService, Role> {

    @AuthorityKey("view")
    public void getRoleMenuTree(){
        renderResult(service.getRoleMenuTree(getKv()));
    }

    /**
     * 更新角色资源权限
     */
    @AuthorityKey("update")
    public void updateRoleResourceAuthority() throws Exception{
        renderResult(service.updateRoleResourceAuthority(getRecordRawData(),getCurrentUser()));
    }
}