package com.jiubanqingchen.future.org.business.module;

import com.jiubanqingchen.future.model.models.Module;
import com.jiubanqingchen.future.org.admin.ModelController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;

@AuthorityKey("org:module")
public class ModuleController extends ModelController<ModuleService, Module> {

}