package com.jiubanqingchen.future.org.authority.userGroupRole;

import com.jiubanqingchen.future.model.models.UserGroupRole;
import com.jiubanqingchen.future.org.admin.ModelController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;

@AuthorityKey("org:userGroupRole")
public class UserGroupRoleController extends ModelController<UserGroupRoleService, UserGroupRole> {

}