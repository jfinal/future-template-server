package com.jiubanqingchen.future.cms.business.dictionarytype;


import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.future.model.models.Dictionary;
import com.jiubanqingchen.future.model.models.DictionaryType;
import com.jiubanqingchen.future.model.models.User;
import com.jiubanqingchen.future.org.admin.ModelService;
import com.jiubanqingchen.future.org.annotation.Sqlkey;

/**
 * @author Light_Dust
 * @ClassName: DictionaryTypeService
 * @Description: (数据字典类别 service)
 * @date 2019年7月4日 下午1:40:19
 */
@Sqlkey("dictionaryType.list")
public class DictionaryTypeService extends ModelService<DictionaryType> {

    @Override
    public void afterDelete(DictionaryType dictionaryType, Record record, User _usr) throws Exception {
        delete(getTableName(Dictionary.class), "codeItemId", dictionaryType.getCodeItemId(), _usr);
        super.afterDelete(dictionaryType, record, _usr);
    }

}
