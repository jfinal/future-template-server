package com.jiubanqingchen.future.org.business.manager;

import com.jfinal.aop.Clear;
import com.jfinal.aop.Inject;
import com.jiubanqingchen.future.org.admin.BaseController;
import com.jiubanqingchen.future.org.authority.user.UserAuthorityService;
import com.jiubanqingchen.future.org.interceptor.AuthorityInterceptor;
import com.jiubanqingchen.future.org.interceptor.OperateLogInterceptor;

public class ManagerController extends BaseController {
    @Inject
    UserAuthorityService userAuthorityService;

    /**
     * 校验是否拥有权限
     */
    @Clear({AuthorityInterceptor.class, OperateLogInterceptor.class})
    public void judgeAuthority(){
        renderResult(userAuthorityService.judgeAuthority(getCurrentUser().getUserId(),getPara("authorityKey")));
    }

    /**
     * 校验是否拥有一组权限
     */
    @Clear({AuthorityInterceptor.class,OperateLogInterceptor.class})
    public void judgeAuthorityArray(){
        String userId=getCurrentUser().getUserId();
        String[] authorityKeys=getParaValues("authorityKeys[]");
        int size=authorityKeys.length;

        boolean[] authorityList=new boolean[size];
        for(int i=0;i<size;i++){
            authorityList[i]= userAuthorityService.judgeAuthority(userId,authorityKeys[i]);
        }
        renderResult(authorityList);
    }

}
