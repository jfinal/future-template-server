package com.jiubanqingchen.future.publish.run;

import com.jfinal.server.undertow.UndertowServer;

public class Run {
    public static void main(String[] args) {
        UndertowServer.create(MainConfig.class, "undertow.properties").configWeb(builder -> {
            builder.addWebSocketEndpoint("com.jiubanqingchen.future.common.tool.WebSocketTool");
        }).start();
    }
}
