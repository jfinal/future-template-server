package com.jiubanqingchen.future.common.enums;

public enum WebSocketCategory {
  NOTICE(30000,"通知"),
  MESSAGE(30001,"消息"),
  AGENCY(30002,"代办"),
  AUTHORITY_CHANGE(30003,"您的权限已被比变更");
  
  private Integer code;

  private String msg;

  private WebSocketCategory(Integer code, String msg) {
      this.code = code;
      this.msg = msg;
  }

  public Integer getCode() {
      return code;
  }

  public void setCode(Integer code) {
      this.code = code;
  }

  public String getMsg() {
      return msg;
  }

  public void setMsg(String msg) {
      this.msg = msg;
  }

}
