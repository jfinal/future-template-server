package com.jiubanqingchen.future.model.models;

import com.jiubanqingchen.future.model.models.base.BaseModule;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class Module extends BaseModule<Module> {
	public static final Module dao = new Module().dao();
}
