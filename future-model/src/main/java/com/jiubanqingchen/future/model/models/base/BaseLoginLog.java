package com.jiubanqingchen.future.model.models.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseLoginLog<M extends BaseLoginLog<M>> extends Model<M> implements IBean {

	public M setLoginLogId(java.lang.String loginLogId) {
		set("loginLogId", loginLogId);
		return (M)this;
	}
	
	public java.lang.String getLoginLogId() {
		return getStr("loginLogId");
	}

	public M setLoginUserName(java.lang.String loginUserName) {
		set("loginUserName", loginUserName);
		return (M)this;
	}
	
	public java.lang.String getLoginUserName() {
		return getStr("loginUserName");
	}

	public M setLoginIp(java.lang.String loginIp) {
		set("loginIp", loginIp);
		return (M)this;
	}
	
	public java.lang.String getLoginIp() {
		return getStr("loginIp");
	}

	public M setLoginArea(java.lang.String loginArea) {
		set("loginArea", loginArea);
		return (M)this;
	}
	
	public java.lang.String getLoginArea() {
		return getStr("loginArea");
	}

	public M setBrowser(java.lang.String browser) {
		set("browser", browser);
		return (M)this;
	}
	
	public java.lang.String getBrowser() {
		return getStr("browser");
	}

	public M setOs(java.lang.String os) {
		set("os", os);
		return (M)this;
	}
	
	public java.lang.String getOs() {
		return getStr("os");
	}

	public M setCreateTime(java.util.Date createTime) {
		set("createTime", createTime);
		return (M)this;
	}
	
	public java.util.Date getCreateTime() {
		return get("createTime");
	}

	public M setCreateUser(java.lang.String createUser) {
		set("createUser", createUser);
		return (M)this;
	}
	
	public java.lang.String getCreateUser() {
		return getStr("createUser");
	}

}
