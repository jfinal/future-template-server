package com.jiubanqingchen.future.org.business.attachment;

import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.future.common.tool.FileTool;
import com.jiubanqingchen.future.model.models.Attachment;
import com.jiubanqingchen.future.model.models.User;
import com.jiubanqingchen.future.org.admin.ModelService;
import com.jiubanqingchen.future.org.annotation.Sqlkey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Sqlkey("attachment.list")
public class AttachmentService extends ModelService<Attachment> {
    private static Logger logger = LoggerFactory.getLogger(AttachmentService.class);
    @Override
    public void afterDelete(Attachment attachment, Record record, User _usr) throws Exception {
        String baseDir = PropKit.get("uploadUrl");// 获取上传文件路径
        String dir = baseDir + attachment.getFilePath();
        logger.info("正在进行文件删除");
        logger.info("文件路径:" + dir);
        // 删除硬盘文件
        FileTool.deleteFile(dir);
        super.afterDelete(attachment, record, _usr);
    }
}
