package com.jiubanqingchen.future.model.generator;

import com.jfinal.kit.PathKit;

public class FutureModelGenerator {

    public static void main(String[] args) {
        // base model 所使用的包名
        String baseModelPackageName = "com.jiubanqingchen.future.model.models.base";
        // base model 文件保存路径
        String baseModelOutputDir = PathKit.getWebRootPath() + "/src/main/java/com/jiubanqingchen/future/model/models/base";

        // model 所使用的包名 (MappingKit 默认使用的包名)
        String modelPackageName = "com.jiubanqingchen.future.model.models";
        // model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
        String modelOutputDir = baseModelOutputDir + "/..";
        QcGenerator.generate(baseModelPackageName,baseModelOutputDir,modelPackageName,modelOutputDir,"future_","future_");
    }
}
