package com.jiubanqingchen.future.common.enums;


/**
 * @ClassName: CacheName  
 * @Description: (ehcache 缓存key枚举)  
 * @author Light_Dust  
 * @date 2019年7月4日 下午1:52:16
 */
public enum CacheName {
	/**toekn缓存*/
	TOKEN_LIST("tokenList"),
	/**用户信息缓存*/
	USER_LIST("userList"),
    /**表格缓存(自动生成模块)*/
    AUTHORITY_LIST("authorityList"),
    /**数据字典缓存*/
    DICTIONARY_LIST("dictionaryList"),
	/**匿名访问清单*/
    ANONYMOUS_LIST("anonymousList");
	
	private String value;
	
	
	private CacheName(String value) {
		this.value=value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	

}
