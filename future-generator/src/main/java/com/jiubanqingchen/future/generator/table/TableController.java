package com.jiubanqingchen.future.generator.table;

import com.jfinal.aop.Inject;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;
import com.jiubanqingchen.future.org.admin.BaseController;

@AuthorityKey("cms:table")
public class TableController extends BaseController {

  @Inject
  TableService tableService;

  /**
   * @Title: listTable
   * @Description: (查询数据库所有列表)
   * @param
   * @return void 返回类型
   * @throws @author Light_dust
   * @date 2019年9月12日 上午10:24:26
   */
  @AuthorityKey("view")
  public void page() throws Exception{
    renderResult(tableService.page( getInt("page",1),getInt("limit",10),getKv()));
  }

  /**
   * @Title: listColumn
   * @Description: (得到当前表的所有字段)
   * @param
   * @return void 返回类型
   * @throws @author Light_dust
   * @date 2019年9月12日 上午10:38:49
   */
  @AuthorityKey("view")
  public void listColumn() {
    renderResult(tableService.listColumn(getPara("tableName")));
  }

}
