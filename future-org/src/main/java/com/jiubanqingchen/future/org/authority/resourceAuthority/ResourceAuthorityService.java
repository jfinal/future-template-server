package com.jiubanqingchen.future.org.authority.resourceAuthority;

import com.jfinal.plugin.activerecord.Db;
import com.jiubanqingchen.future.model.models.ResourceAuthority;
import com.jiubanqingchen.future.org.admin.ModelService;
import com.jiubanqingchen.future.org.annotation.Sqlkey;

import java.util.List;

@Sqlkey("resourceAuthority.list")
public class ResourceAuthorityService extends ModelService<ResourceAuthority> {
    /**
     * 获取角色的资源信息
     * @param roleId 角色id
     * @return
     */
    public List<String> getResourceIdsByRoleId(String roleId){
       return  Db.query("SELECT resourceId FROM future_resource_authority where roleId=?",roleId);
    }
	
}