package com.jiubanqingchen.future.common.tool;

import com.jfinal.kit.StrKit;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StrTool {


  /**
   * @Title: UnderlineToHump
   * @Description: (下划线转驼峰命名)
   * @param @param str
   * @param @return 参数
   * @return String 返回类型
   * @throws @author Light_Dust
   * @date 2019年5月11日 下午1:52:58
   */
  public static String underlineToHump(String str) {
    if (!str.contains("_")) {
      return str.toLowerCase();
    }
    String[] names = str.split("_");
    StringBuilder result = new StringBuilder();
    for (String name : names) {
      if (result.length() == 0) {
        result.append(name.toLowerCase());
      } else {
        result.append(StrKit.firstCharToUpperCase(name));
      }
    }
    return result.toString();
  }

  /**
   * @Title: getTableModelName
   * @Description: (得到表名的视图name,例：admin_dictionary_type dictionaryType 去除前缀 转驼峰命名)
   * @param @return 参数
   * @return String 返回类型
   * @throws @author Light_Dust
   * @date 2019年5月18日 上午10:51:43
   */
  public static String getTableModelName(String tableName) {
    return underlineToHump(tableName.substring(tableName.indexOf("_") + 1));
  }

  /**
   * @Title: delHtmlTag
   * @Description: (去除htmlTag)
   * @param @return 参数
   * @return String 返回类型
   * @throws @author Light_Dust
   * @date 2019年5月18日 上午10:51:43
   */
  public static String delHtmlTag(String htmlStr) {
    String regEx_script = "<script[^>]*?>[\\s\\S]*?<\\/script>"; // 定义script的正则表达式
    String regEx_style = "<style[^>]*?>[\\s\\S]*?<\\/style>"; // 定义style的正则表达式
    String regEx_html = "<[^>]+>"; // 定义HTML标签的正则表达式

    Pattern p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
    Matcher m_script = p_script.matcher(htmlStr);
    htmlStr = m_script.replaceAll(""); // 过滤script标签

    Pattern p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
    Matcher m_style = p_style.matcher(htmlStr);
    htmlStr = m_style.replaceAll(""); // 过滤style标签

    Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
    Matcher m_html = p_html.matcher(htmlStr);
    htmlStr = m_html.replaceAll(""); // 过滤html标签

    return htmlStr.trim(); // 返回文本字符串
  }

  /**
   * @Title: arrayToString
   * @Description: (String[1,2] ==> String 1,2)
   * @param @return 参数
   * @return String 返回类型
   * @throws @author Light_dust
   * @date 2019年9月23日 上午1:21:19
   */
  public static String arrayToString(String[] array) {
      StringBuilder sb = new StringBuilder();
      if (array != null && array.length > 0) {
        for (int i = 0; i < array.length; i++) {
          if (i < array.length - 1) {
            sb.append(array[i] + ",");
          } else {
            sb.append(array[i]);
          }
        }
        return sb.toString();
      }
      return null;
  }
}
