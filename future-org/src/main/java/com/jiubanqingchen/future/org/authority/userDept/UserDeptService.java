package com.jiubanqingchen.future.org.authority.userDept;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jiubanqingchen.future.model.models.User;
import com.jiubanqingchen.future.model.models.UserDept;
import com.jiubanqingchen.future.org.admin.ModelService;
import com.jiubanqingchen.future.org.annotation.Sqlkey;

@Sqlkey("userDept.list")
public class UserDeptService extends ModelService<UserDept> {

    @Override
    public void preUpdate(UserDept userDept, Record record, User _usr) throws Exception {
        String updateKey = record.getStr("updateKey");
        if ("main".equals(updateKey))
            Db.update("update future_user_dept set main=0 where userId=?", userDept.getUserId());
        super.preUpdate(userDept, record, _usr);
    }
}