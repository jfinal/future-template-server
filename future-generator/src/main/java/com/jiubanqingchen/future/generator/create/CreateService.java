package com.jiubanqingchen.future.generator.create;

import cn.hutool.core.date.DateUtil;
import com.jfinal.aop.Inject;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.template.Engine;
import com.jiubanqingchen.future.common.tool.FileTool;
import com.jiubanqingchen.future.generator.table.TableService;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class CreateService {
    private static Engine engine;// 得到engine对象
    @Inject
    TableService tableService;

    private void loadConfig() {
        if (engine == null) {
            String url = this.getClass().getResource("/future-generator/template/").toString();
            if (url.indexOf("file:/") >= 0) url = url.substring(6);
            engine = Engine.use().setBaseTemplatePath(url);
        }
    }

    public Engine getCreateEngine() {
        if (engine == null) loadConfig();
        return engine;
    }

    /**
     * @param @param  tableName 表名
     * @param @return 参数
     * @return String 返回类型
     * @throws @author Light_dust
     * @Title: getPrimaryName
     * @Description: (得到主键, 默认取第一个)
     * @date 2019年9月19日 下午1:41:19
     */
    private String getPrimaryName(String tableName) {
        return tableService.getPrimaryColumn(tableName).get(0).getStr("primaryName");
    }

    /**
     * @param @param  kv
     * @param @return 参数
     * @return Kv 返回类型
     * @throws @author Light_dust
     * @Title: preCreate
     * @Description: (插入前操作)
     * @date 2019年9月19日 下午7:02:46
     */
    private Record preCreate(Record createForm) {
        createForm.set("upperBeanName", StrKit.firstCharToUpperCase(createForm.getStr("beanName")));
        createForm.set("nowDate", DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        List<Map<String, Object>> list = createForm.get("columns");
        for (Map<String, Object> map : list) {
            if ("true".equals(map.get("showTable").toString())) {
                createForm.set("customRender", "custom" + StrKit.firstCharToUpperCase(map.get("name").toString()));
                break;
            }
        }
        return createForm;
    }

    /**
     * @param @param kv 参数
     * @return void 返回类型
     * @throws @author Light_dust
     * @Title: create
     * @Description:(生成文件)
     * @date 2019年9月16日 下午9:29:10
     */
    public void create(Record createForm) {
        loadConfig();
        createForm = preCreate(createForm);
        String type = createForm.getStr("createType");
        switch (type) {
            case "all":
                createAll(createForm);
                break;
            case "server":
                createServer(createForm);
                break;
            case "web":
                createWeb(createForm);
                break;
            case "java":
                createJava(createForm);
                break;
            case "sql":
                createSql(createForm);
                break;
            case "api":
                createApi(createForm);
                break;
            case "vue":
                createHtml(createForm);
                break;
            case "profile":
                createProfile(createForm);
                break;
            default:
                break;
        }
    }

    /**
     * @param @param tableName 表名
     * @param @param module 模块名称
     * @return void 返回类型
     * @throws @author Light_dust
     * @Title: createApi
     * @Description: (生成API.js)
     * @date 2019年9月16日 下午1:19:51
     */
    public void createApi(Record createForm) {
        String dir = createForm.getStr("apiCreatePath") + "/";
        String fileName = createForm.getStr("beanName") + ".js";
        engine.getTemplate("api.html").render(createForm.getColumns(), FileTool.createFile(dir, fileName));
    }

    /**
     * @param @param kv 参数
     * @return void 返回类型
     * @throws @author Light_dust
     * @Title: createSql
     * @Description: (生成sql)
     * @date 2019年9月19日 下午1:44:43
     */
    public void createSql(Record createForm) {
        createForm.set("primaryName", getPrimaryName(createForm.getStr("tableName")));
        String dir = createForm.getStr("sqlCreatePath") + "/";
        String fileName = createForm.getStr("beanName") + ".sql";
        engine.getTemplate("sql.html").render(createForm.getColumns(), FileTool.createFile(dir, fileName));
    }

    /**
     * @param @param kv 参数
     * @return void 返回类型
     * @throws @author Light_dust
     * @Title: createJava
     * @Description: (创建java文件)
     * @date 2019年9月19日 下午7:02:12
     */
    public void createJava(Record createForm) {
        String fileName = createForm.getStr("upperBeanName");// 文件名
        String dir = createForm.getStr("javaCreatePath") + "/";
        engine.getTemplate("controller.html").render(createForm.getColumns(),
                FileTool.createFile(dir, fileName + "Controller.java"));
        engine.getTemplate("service.html").render(createForm.getColumns(),
                FileTool.createFile(dir, fileName + "Service.java"));
    }

    /**
     * @param @param kv 参数
     * @return void 返回类型
     * @throws @author Light_dust
     * @Title: createIndex
     * @Description: (创建index页面)
     * @date 2019年10月7日 下午6:35:54
     */
    public void createIndex(Record createForm) {
        engine.getTemplate("index.html").render(createForm.getColumns(), FileTool.createFile(createForm.getStr("dir"), "list.vue"));
    }

    /**
     * @param @param kv 参数
     * @return void 返回类型
     * @throws @author Light_dust
     * @Title: createForm
     * @Description: (创建form页面)
     * @date 2019年10月7日 下午6:36:18
     */
    public void createForm(Record createForm) {
        String templateUrl = "form.html";
        engine.getTemplate(templateUrl).render(createForm.getColumns(), FileTool.createFile(createForm.getStr("dir"), "form.vue"));
    }

    /**
     * @param @param kv 参数
     * @return void 返回类型
     * @throws @author Light_dust
     * @Title: createProfile
     * @Description: (创建profile js)
     * @date 2020年7月5日 下午6:36:18
     */
    public void createProfile(Record createForm) {
        String dir = createForm.getStr("profileCreatePath") + "/";
        String fileName = createForm.getStr("beanName") + ".js";
        engine.getTemplate("profile.html").render(createForm.getColumns(), FileTool.createFile(dir, fileName));
    }


    /**
     * @param @param kv 参数
     * @return void 返回类型
     * @throws @author Light_dust
     * @Title: createHtml
     * @Description: (创建html页面)
     * @date 2019年10月7日 下午6:27:13
     */
    public void createHtml(Record createForm) {
        String dir = createForm.getStr("vueCreatePath") + "/";
        String tableName = createForm.getStr("tableName");
        createForm.set("primaryName", getPrimaryName(tableName));
        createForm.set("dir", dir);
        createIndex(createForm);
        createForm(createForm);
        createProfile(createForm);
    }

    /**
     * @param @param kv 参数
     * @return void 返回类型
     * @throws @author Light_dust
     * @Title: createServer
     * @Description: (创建服务端数据)
     * @date 2019年9月19日 下午7:06:08
     */
    public void createServer(Record createForm) {
        createJava(createForm);
        createSql(createForm);
    }

    /**
     * @param @param kv 参数
     * @return void 返回类型
     * @throws @author Light_dust
     * @Title: createWeb
     * @Description: (生成页面数据)
     * @date 2019年9月19日 下午7:15:35
     */
    public void createWeb(Record createForm) {
        createApi(createForm);
        createHtml(createForm);
    }

    /**
     * @param @param kv 参数
     * @return void 返回类型
     * @throws @author Light_dust
     * @Title: createAll
     * @Description:(生成所有的)
     * @date 2019年9月19日 下午7:18:06
     */
    public void createAll(Record createForm) {
        createServer(createForm);
        createWeb(createForm);
    }
}
