package com.jiubanqingchen.future.common.tool;

import com.jfinal.kit.StrKit;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Tool {
	 /**
     * @Title: md5  
     * @Description: (得到str的MD5值)  
     * @param @param str
     * @param @return    参数  
     * @return String    返回类型  
     * @throws  
     * @author Administrator  
     * @date 2018年10月15日 下午2:10:11
     */
    public static String md5(String str,String salt) {
      
      if(StrKit.isBlank(salt)) {
        salt="jiubanqingchen";
      }
      //得到加盐后的字符串
      String md5Str=str+salt;
        //定义一个字节数组
        byte[] secretBytes = null;
        try {
              // 生成一个MD5加密计算摘要  
            MessageDigest md = MessageDigest.getInstance("MD5");
            //对字符串进行加密
            md.update(md5Str.getBytes());
            //获得加密后的数据
            secretBytes = md.digest();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("没有md5这个算法！");
        }
        //将加密后的数据转换为16进制数字
        String md5code = new BigInteger(1, secretBytes).toString(16);// 16进制数字
        // 如果生成数字未满32位，需要前面补0
        for (int i = 0; i < 32 - md5code.length(); i++) {
            md5code = "0" + md5code;
        }
        return md5code;
    }
    
    public static String md5(String str) {
    	return md5(str,null);
    }
}
