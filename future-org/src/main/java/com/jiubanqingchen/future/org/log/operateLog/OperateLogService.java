package com.jiubanqingchen.future.org.log.operateLog;

import com.jiubanqingchen.future.model.models.OperateLog;
import com.jiubanqingchen.future.org.admin.ModelService;
import com.jiubanqingchen.future.org.annotation.Sqlkey;

@Sqlkey("operateLog.list")
public class OperateLogService extends ModelService<OperateLog> {
	
	
}