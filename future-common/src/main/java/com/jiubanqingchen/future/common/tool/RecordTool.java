package com.jiubanqingchen.future.common.tool;

import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Record;

import java.util.ArrayList;
import java.util.List;

/**
 * record 工具类
 *
 * @author light_dust
 * @Date 2020/5/23
 */
public class RecordTool {

    /**
     * 将recordList转化为ClassList
     *
     * @param recordList
     * @param clazz
     * @return
     * @throws Exception
     */
    public static <T> List<T> recordListToBeanList(List<Record> recordList, Class<T> clazz) {
        if (recordList == null || recordList.size() == 0) return null;
        List<T> list = new ArrayList<>();
        recordList.forEach(record -> {
            list.add(recordToBean(record, clazz));
        });
        return list;
    }

    /**
     * record 转为 bean
     *
     * @param record
     * @param clazz
     * @return
     */
    public static <T> T recordToBean(Record record, Class<T> clazz) {
        return record == null ? null : JsonKit.parse(JsonKit.toJson(record.getColumns()), clazz);
    }

}
