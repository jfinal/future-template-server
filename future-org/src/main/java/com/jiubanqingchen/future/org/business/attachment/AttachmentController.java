package com.jiubanqingchen.future.org.business.attachment;

import com.jiubanqingchen.future.model.models.Attachment;
import com.jiubanqingchen.future.org.admin.ModelController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;

@AuthorityKey("org:attachment")
public class AttachmentController extends ModelController<AttachmentService, Attachment> {

}