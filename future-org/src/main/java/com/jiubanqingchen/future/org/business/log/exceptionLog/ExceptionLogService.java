package com.jiubanqingchen.future.org.business.log.exceptionLog;

import com.jiubanqingchen.future.model.models.ExceptionLog;
import com.jiubanqingchen.future.org.admin.ModelService;
import com.jiubanqingchen.future.org.annotation.Sqlkey;

@Sqlkey("exceptionLog.list")
public class ExceptionLogService extends ModelService<ExceptionLog> {

}