package com.jiubanqingchen.future.cms.business.dictionarytype;

import com.jiubanqingchen.future.model.models.DictionaryType;
import com.jiubanqingchen.future.org.admin.ModelController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;


/**
 * @author Light_Dust
 * @ClassName: DictionaryTypeController
 * @Description: (字典类型别)
 * @date 2019年7月4日 下午1:36:13
 */
@AuthorityKey("cms:dictionaryType")
public class DictionaryTypeController extends ModelController<DictionaryTypeService, DictionaryType> {

}
