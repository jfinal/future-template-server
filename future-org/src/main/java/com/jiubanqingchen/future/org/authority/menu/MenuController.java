package com.jiubanqingchen.future.org.authority.menu;

import com.jiubanqingchen.future.model.models.Menu;
import com.jiubanqingchen.future.org.admin.ModelController;
import com.jiubanqingchen.future.org.annotation.AuthorityKey;

@AuthorityKey("org:menu")
public class MenuController extends ModelController<MenuService, Menu> {

    @AuthorityKey("view")
    public void menuTree() {
        renderResult(service.menuTree(getKv()));
    }
}
